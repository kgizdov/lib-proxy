/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#ifndef INCLUDE_MAPMTFEBCSR_H_
#define INCLUDE_MAPMTFEBCSR_H_

union mac_u {
  unsigned char uc[6];
  unsigned short us[3];
};

union ip_u {
  unsigned char uc[4];
  unsigned short us[2];
  unsigned int ui;
};

struct channel_ctl_t {
  unsigned short inhb :1;
  unsigned short mode :1;
  unsigned short zs :1;
  unsigned short suppress_empty :1;
  unsigned short emulator_inhb :1;
  unsigned short emulator_mode :1;
  unsigned short :1;
  unsigned short emulator_enable :1;
  unsigned short mask_mode :1;
  unsigned short :7;
};

struct cmd_t {
  unsigned short type :10;
  unsigned short :2;
  unsigned short channel :4;
};

struct bits_t {
  unsigned short pattern :4;
  unsigned short address :11;
  unsigned short terminator :1;
};

struct egress_ctl_t {
  unsigned short mep_static_destination :1;
  unsigned short mep_no_tfc :1;
  unsigned short tfc_throttle_0 :1;
  unsigned short tfc_throttle_1 :1;
  unsigned short tfc_throttle_0_polarity :1;
  unsigned short tfc_throttle_1_polarity :1;
  unsigned short tfc_decoder_reset_b :1;
  unsigned short gbe_poll_enable :1;
  unsigned short gbe_port :2;
  unsigned short gbe_static_port :1;
  unsigned short :5;
};

struct egress_ctl2_t {
  unsigned short :4;
  unsigned short lhcb_alice :1;
  unsigned short :11;
};

struct resets_t {
  unsigned short ingress_status_rd :1;
  unsigned short ingress_status_wr :1;
  unsigned short l1 :1;
  unsigned short dcms :1;
  unsigned short :12;
};

struct ingress_ctl_t {
  unsigned short inhibit :4;
  unsigned short cfg_mbx_wrrst :1;
  unsigned short cfg_mbx_rdrst :1;
  unsigned short cfg_xfer :1;
  unsigned short cfg_broadcast :1;
  unsigned short cfg_target :2;
  unsigned short :6;
};


struct mapmtfebControl_t {
  mac_u macdst;
  mac_u macsrc;
  ip_u ipdst;
  ip_u ipsrc;
  unsigned short ip_protocol;
  unsigned short ip_tos;
  unsigned short ip_ttl;
  unsigned short ip_type;
  unsigned short ip_version;
  egress_ctl_t egress_ctl;
  unsigned partition;
  unsigned short mep_event_count;
  egress_ctl2_t egress_ctl2;
  unsigned short unused1;
  resets_t resets;
  unsigned short tfc_latency;
  unsigned short l1_id;
  ingress_ctl_t ingress_ctl;
  unsigned short unused2[4];
  unsigned short mep_max_fragment;
  unsigned short mep_hwm;
  unsigned short ingress_mbx;
};

union mapmtfebControl_u_t {
  unsigned char uc[64];
  mapmtfebControl_t ctl;
};

struct l1pulser_ctl_t {
  unsigned short bgo :1;
  unsigned short bmode :1;
  unsigned short auto_bgo :1;
  unsigned short external_trigger_enable :1;
  unsigned short external_trigger_clocked :1;
  unsigned short poison :1;
  unsigned short go :1;
  unsigned short trigger :1;
  unsigned short veto_enable :1;
  unsigned short :7;
};

struct ttcBData_t {
  unsigned subaddr_data :16;
  unsigned high :1;
  unsigned external :1;
  unsigned ttcrx_addr :14;
};

struct l1pulser_t {
  unsigned short burst_count;
  unsigned short burst_interval;
  unsigned short burst_delay;
  unsigned short pulse_count;
  unsigned short pulse_interval;
  l1pulser_ctl_t control;
  ttcBData_t ttc_d;
};

//
// Status registers
//

// Ingress channel status is same for 12ch and 36ch boards

struct l1ChannelGeneralStatus_t {
  unsigned short rx_los :2;             // 0=synced,1=resyncing,2=sync_lost
  unsigned short rxbuffer_half_full :1;
  unsigned short rxbuffer_overflow :1;
  unsigned short gt_inhibit :1;
  unsigned short cosync_error :1;
  unsigned short ingress_buffer_ready :1;
  unsigned short ingress_buffer_full :1;
  unsigned short :8;
};

struct l1ChannelStatus_t {
  l1ChannelGeneralStatus_t status;
  unsigned char cosync_count;
  unsigned char rx_disperr_count;
  unsigned char rx_clkcor_count;
  unsigned char rx_buferr_count;
  unsigned short cfg;
  unsigned char zsfifo_out_count;
  unsigned char zsfifo_in_count;
  unsigned char zs_count;
  unsigned char ingress_count;
  unsigned char zs_b_count;
  unsigned char zs_a_count;
  unsigned char latency_l0;
  unsigned char latency_tfc;
};


struct mapmtfebStatus_t {
  //0
  unsigned short ttcid : 8;
  unsigned short tfcFifoRequest : 1;
  unsigned short ttcReady : 1;
  unsigned short :2;
  unsigned short evtMergerReadState :4;

  //1
  unsigned short gttxrx0Inhibit : 4;
  unsigned short gttxrx1Inhibit : 4;
  unsigned short Inhibit : 4;
  unsigned short gttxrx2Inhibit : 4;

  //2
  unsigned short spi3TxBusy : 1;
  unsigned short mepFifoFull :1;
  unsigned short throttleOr : 2;
  unsigned short gtFifoFull :4;
  unsigned short gtHighTxReady :4;
  unsigned short gtLowTxReady :4;

  //3
  unsigned short tfcL0Reset :1;
  unsigned short tfcL1Reset :1;
  unsigned short tfcBShort :6;
  unsigned short tpa :4;
  unsigned short fragmentReady :1;
  unsigned short mepReady :1;
  unsigned short fragmentReq :1;
  unsigned short evtQValid :1;

  //4
  unsigned short fragmenterState :8;
  unsigned short :8;

  //5
  unsigned short ethQValid :1;
  unsigned short aSequenceError :1;
  unsigned short mepCount :2;
  unsigned short gtFifoTxWait :4;
  unsigned short tfcFifoOccupancy:8;

  //6
  unsigned short tfcBLong :16;

  //7
  unsigned short gtin_rxseqerr_0 :2;
  unsigned short gtin_rxseqerr_1 :2;
  unsigned short gtin_rxseqerr_2 :2;
  unsigned short gtin_rxseqerr_3 :2;
  unsigned short gtin_txseqerr_0 :2;
  unsigned short gtin_txseqerr_1 :2;
  unsigned short gtin_txseqerr_2 :2;
  unsigned short gtin_txseqerr_3 :2;

  //8
  unsigned short fe0;
  unsigned short fe1;
  unsigned short fe2;
  unsigned short fe3;

  //12
  unsigned short tfcACount :8;
  unsigned short tfcRotCount :8;

  //13
  unsigned short tfcMepCount :8;
  unsigned short mepSequenceCount :4;
  unsigned short :4;

  //14
  unsigned short mepIngressPollCount :8;
  unsigned short gtinTxWaitEOF_0 :1;
  unsigned short gtinTxWaitEOF_1 :1;
  unsigned short gtinTxWaitEOF_2 :1;
  unsigned short gtinTxWaitEOF_3 :1;
  unsigned short :4;

  //15
  unsigned short alarms;

  //16
  unsigned short gtinOccupancy_0_0 :4;
  unsigned short gtinOccupancy_0_1 :4;
  unsigned short gtinOccupancy_1_0 :4;
  unsigned short gtinOccupancy_1_1 :4;
  unsigned short gtinOccupancy_2_0 :4;
  unsigned short gtinOccupancy_2_1 :4;
  unsigned short gtinOccupancy_3_0 :4;
  unsigned short gtinOccupancy_3_1 :4;

  //18
  unsigned short gtinRxCount_0_0 :4;
  unsigned short gtinRxCount_0_1 :4;
  unsigned short gtinRxCount_1_0 :4;
  unsigned short gtinRxCount_1_1 :4;
  unsigned short gtinRxCount_2_0 :4;
  unsigned short gtinRxCount_2_1 :4;
  unsigned short gtinRxCount_3_0 :4;
  unsigned short gtinRxCount_3_1 :4;

  //20
  unsigned short gtinTxCount_0_0 :4;
  unsigned short gtinTxCount_0_1 :4;
  unsigned short gtinTxCount_1_0 :4;
  unsigned short gtinTxCount_1_1 :4;
  unsigned short gtinTxCount_2_0 :4;
  unsigned short gtinTxCount_2_1 :4;
  unsigned short gtinTxCount_3_0 :4;
  unsigned short gtinTxCount_3_1 :4;

  //22
  unsigned short gtinLastLength_0 :4;
  unsigned short gtinLastLength_1 :4;
  unsigned short gtinLastLength_2 :4;
  unsigned short gtinLastLength_3 :4;

  //23
  unsigned short formatter_fragments_out :8;
  unsigned short ethsa_fragments_out :8;

  //24
  unsigned short spi3txTeopCount :8;
  unsigned short spi3txTsxCount :8;

  //25
  unsigned short ethsaState :3;
  unsigned short :5;
  unsigned short evtMergerWriteState :5;
  unsigned short :3;
};

union mapmtfebStatus_u_t {
  unsigned char uc[64];
  mapmtfebStatus_t status;
};

struct l1StatusFE_t {
  unsigned short reset : 1;
  unsigned short ready_b :1;
  unsigned short lockedUp : 1;
  unsigned short lockedDown : 1;
  unsigned short dcmsLocked_b : 1;
  unsigned short gttxrx0Inhibit : 1;
  unsigned short gttxrx1Inhibit : 1;
  unsigned short gttxrx2Inhibit :1;
  unsigned short tfcFifoReady : 1;
  unsigned short tfcFifoFull :1;
  unsigned short egressBusy :1;
  unsigned short mrxSd :1;
  unsigned short :4;

  unsigned short :16;

  unsigned short tfcFifoCountIn :8;
  unsigned short tfcFifoCountOut :8;

  unsigned short :8;
  unsigned short tfcFifoCount :8;
};

union l1StatusFE_u {
  unsigned short reg[4];
  l1StatusFE_t status;
};

#endif  // INCLUDE_MAPMTFEBCSR_H_
