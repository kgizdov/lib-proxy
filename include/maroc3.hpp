/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#ifndef __MAROC3_HPP__
#define __MAROC3_HPP__

#include <stdint.h>
#include <string>
#include <sstream>
#include "./custom.hpp"

#include "./maroc3csr.h"
#include "./tinyxml2.h"

// Helper functions to access the MAROC register map

class MAROC3 {
 public:
    MAROC3();
    // void setStateFromPreferences(Preferences topPrefs, int db);  // reinmplement in C++
    // void setStateFromPreferences();
    // int setStateFromPreferences(tinyxml2::XMLDocument * doc, tinyxml2::XMLDocument * marocPD);
    int setStateFromPreferences(tinyxml2::XMLDocument *doc, tinyxml2::XMLDocument *marocPD, int32_t db);
    int setGlobalGain(tinyxml2::XMLDocument *marocPD, pmt_t pmt[], int32_t db);
    int setGlobalCTest(tinyxml2::XMLDocument *marocPD, bool testpulse, int32_t db);

    void getRegisterArray(int mar, byte regs[]);
    void getRegisterArray(int mar, byte regs[], int nreg, int offset);
    void set_small_dac(int i, int mar);

    int get_small_dac(int mar);  // test purposes

    void set_enb_outADC(int i, int mar);
    void set_inv_startCmptGray(int i, int mar);
    void set_ramp_8bit(int i, int mar);
    void set_ramp_10bit(int i, int mar);
    void set_cmd_CK_mux(int i, int mar);
    void set_d1_d2(int i, int mar);
    void set_inv_discriADC(int i, int mar);
    void set_polar_discri(int i, int mar);
    void set_enb_tristate(int i, int mar);
    void set_valid_dc_fsb2(int i, int mar);
    void set_sw_fsb2_50f(int i, int mar);
    void set_sw_fsb2_100f(int i, int mar);
    void set_sw_fsb2_100k(int i, int mar);
    void set_sw_fsb2_50k(int i, int mar);
    void set_valid_dc_fs(int i, int mar);
    void set_cmd_fsb_fsu(int i, int mar);
    void set_sw_fsb1_50f(int i, int mar);
    void set_sw_fsb1_100f(int i, int mar);
    void set_sw_fsb1_100k(int i, int mar);
    void set_sw_fsb1_50k(int i, int mar);
    void set_sw_fsu_100k(int i, int mar);
    void set_sw_fsu_50k(int i, int mar);
    void set_sw_fsu_25k(int i, int mar);
    void set_sw_fsu_40f(int i, int mar);
    void set_sw_fsu_20f(int i, int mar);
    void set_h1h2_choice(int i, int mar);
    void set_en_adc(int i, int mar);
    void set_sw_ss_1200f(int i, int mar);
    void set_sw_ss_600f(int i, int mar);
    void set_sw_ss_300f(int i, int mar);
    void set_onoff_ss(int i, int mar);
    void set_swb_buf_2p(int i, int mar);
    void set_swb_buf_1p(int i, int mar);
    void set_swb_buf_500f(int i, int mar);
    void set_swb_buf_250f(int i, int mar);
    void set_cmd_fsb(int i, int mar);
    void set_cmd_ss(int i, int mar);
    void set_cmd_fsu(int i, int mar);

    void set_cmd_sum(int ch, int i, int mar);
    void set_gain(int ch, int i, int mar);

    void set_dac(int dac, int i, int mar);

    void set_ctest(int ch, int i, int mar);
    void set_mask_or1(int ch, int i, int mar);
    void set_mask_or2(int ch, int i, int mar);

 private:
    maroc3Control_u_t m3[2];
    maroc3SumGain_u_t m3sg[64];
    unsigned maroc3MaskOr[4];
};

#endif  // __MAROC3_HPP__
