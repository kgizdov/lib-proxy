// Copyright 2016 Konstantin Gizdov University of Edinburgh

#ifndef __CUSTOM_HPP__
#define __CUSTOM_HPP__

#include <stdint.h>
#include <errno.h>

#include <string>
#include <vector>

#define getVarName(var) #var

typedef uint8_t byte;       // define 8 bit byte
typedef uint16_t byte16_t;  // define 16 bit byte

const uint32_t bufSendSize = 388;    // define for later simplicity
const uint32_t kBufSendSize = 388;
const uint32_t proxyMsgSize = 1024;  // define for later simplicity
const uint32_t kProxyMsgSize = 1024;

union byteint {
    byte b[sizeof(int32_t)];
    int32_t i;
};

union byteshort {
    byte b[sizeof(int16_t)];
    int16_t i;
};

union byteuint {
    byte b[sizeof(uint32_t)];
    uint32_t i;
};

union byteushort {
    byte b[sizeof(uint16_t)];
    uint16_t i;
};

union over {
    struct info {
        int32_t ask;
        int32_t tell;
    } info;
    byte b[sizeof(info)];
};

struct pmt_t {
    int32_t pos;
    int32_t gain;
};

struct dna_t {
    int32_t db;
    std::string name;
    std::string file;
    std::string folder;
    std::string mac;
    std::string ip;
    pmt_t pmt[2];
    int32_t trig;
    bool testpulse;
    int32_t dac;
    int32_t hold;
    int32_t on;
};

struct pc_t {
    std::string mac;
    std::string ip;
};

struct msg_t {
    uint16_t length;  // length of following message
    uint16_t comm;    // current command type
    uint32_t info;    // accompanying info
    uint32_t seq;     // exchange sequence number
};  // size 12 bytes

union msg_u {
    msg_t inf;
    byte b[sizeof(msg_t)];
};  // size 12 bytes

bool checkIPAddress(const std::string &ip);

bool checkMACAddress(const char *mac);

std::string operator+(const std::string &lstring, int int_val);
std::string operator+(const std::string &lstring, unsigned uint_val);

int handle_join(const std::string &thread, int rc);

#endif  // __CUSTOM_HPP__
