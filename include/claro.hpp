/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
Konstantin Gizdov (Uni-Edinburgh)
*/

#ifndef __CLARO_HPP__
#define __CLARO_HPP__

#include <stdint.h>
#include "./custom.hpp"
#include "./clarocsr.h"
#include "./tinyxml2.h"

// Helper functions to access the CLARO register map

class CLARO {
 public:
    void setStateFromPreferences(Preferences topPrefs, int db);
    void setStateFromPreferences(tinyxml2::XMLDocument *doc, tinyxml2::XMLDocument *docName, const commonPrefsStruct &commonPrefs);

    int getNregisters();
    void getRegisterArray(int regs[], int cla);
    void getRegisterArray(int cla, byte regs[], int nreg, int offset);

    // 'global' settings
    void set_mux(int i, int cla);
    void set_SEUEn(int i, int cla);
    void set_internCorr(int i, int cla);
    void set_auxTP(int i, int cla);
    void set_TPEn(int i, int cla);
    void set_SEUreset(int i, int cla);

    void set_threshold(int ch, int i, int cla);
    void set_attenuation(int ch, int i, int cla);
    void set_inputEnabled(int ch, int i, int cla);
    void set_hysteresisDisabled(int ch, int i, int cla);
    void set_thresholdOffset(int ch, int i, int cla);
    void set_TPDisabled(int ch, int i, int cla);
};

#endif  // __CLARO_HPP__
