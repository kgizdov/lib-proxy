// Copyright 2016 Konstantin Gizdov University of Edinburgh

#ifndef __XML_HELPERS_HPP__
#define __XML_HELPERS_HPP__

#include <string>
#include <iostream>

#include "./tinyxml2.h"


#ifndef XMLCheckSuccess
    #define XMLCheckSuccess(errCheck) if (errCheck != tinyxml2::XML_SUCCESS) { fprintf(stderr, "Error: %i\n", errCheck); return errCheck; }
#endif

// extern tinyxml2::XMLDocument settings, prefsdoc, chimDNA, claroPD, marocPD, rich_pckd, richXY, labels;
extern tinyxml2::XMLDocument settings, prefsdoc, marocPD;

void lookupXMLValue();

tinyxml2::XMLDocument *newXMLDoc();

int addElementToDoc(tinyxml2::XMLDocument *doc, std::string name);

int addCommentToDoc(tinyxml2::XMLDocument *doc, std::string comment);

int addElementToNode(tinyxml2::XMLNode *node, std::string name);

void setElementKeyAndValue(tinyxml2::XMLElement *elem, std::string keyName, std::string value);

int setValueOfElementKey(tinyxml2::XMLElement *elem, std::string keyName, std::string value);

int moveToNode(tinyxml2::XMLNode *input_node, const std::string &name);
tinyxml2::XMLNode *moveToNode2(tinyxml2::XMLNode *input_node, const std::string &name);

int moveToInnerNode(tinyxml2::XMLNode *input_node, const std::string &name);
tinyxml2::XMLNode *moveToInnerNode2(tinyxml2::XMLNode *input_node, const std::string &name);

tinyxml2::XMLNode *getNodeByName(tinyxml2::XMLDocument*doc, const std::string &name);

tinyxml2::XMLElement *getElementByName(tinyxml2::XMLDocument *doc, std::string elemName);

tinyxml2::XMLElement *getElementByNameFromNode(tinyxml2::XMLNode *input_node, std::string const &elemName);

tinyxml2::XMLElement *getElementByProperty(tinyxml2::XMLDocument *doc, std::string const &property_name);

tinyxml2::XMLElement *getElementByPropertyFromNode(tinyxml2::XMLNode *input_node, std::string const &property_name);

int getIntPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, int &return_value, int default_value);
int getIntPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, int *return_value, int default_value);

int getUnsignedPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned &return_value, unsigned default_value);
int getUnsignedPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned *return_value, unsigned default_value);

int getBoolPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, bool &return_value, bool default_value);
int getBoolPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, bool*return_value, bool default_value);

int getStingPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string &return_value, std::string default_value);
int getStingPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string *return_value, std::string default_value);

int getIntPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, int &return_value, int default_value);
int getIntPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, int *return_value, int default_value);

int getUnsignedPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, unsigned &return_value, unsigned default_value);
int getUnsignedPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, unsigned *return_value, unsigned default_value);

int getBoolPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, bool &return_value, bool default_value);
int getBoolPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, bool *return_value, bool default_value);

int getStringPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, std::string &return_value, std::string default_value);
int getStringPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, std::string *return_value, std::string default_value);

int setIntPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, int value);
int setIntPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, int value);

int setUnsignedPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned value);
int setUnsignedPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, unsigned value);

int setBoolPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, bool value);
int setBoolPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, bool value);

int setStringPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string value);
int setStringPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, std::string value);


// print all attributes of an element.
// returns the number of attributes
// int print_attr(tinyxml2::XMLElement*element, unsigned int indent);

// print the names of all children of an element
// returns the number of elements
// int print_elem(tinyxml2::XMLElement*element, unsigned int indent);

#endif  // __XML_HELPERS_HPP__
