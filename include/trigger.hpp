/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#ifndef __TRIGGER_HPP__
#define __TRIGGER_HPP__

#include <string>
#include <vector>

#include "./adept_depp.hpp"

class TriggerBoard {
 public:
    TriggerBoard();
    ~TriggerBoard();
    explicit TriggerBoard(std::string deviceName);

    void initTrigger();
    bool reset();
    void setSwitchBits(int bits, int mask);
    void setDeadtime(int deadtime);
    void changeDeadtime(int deadtime);
    void setPulseDelay(int pulseDelay);
    void changePulseDelay(int pulseDelay);
    void resetCounts();
    void toggleButtonBits(int bits);
    void updateCount();
    int getCount();
    std::vector<int> getCounts();
    int getCount(int id);

    void updateGateCA();

    int setOnOff(bool on, int which);
    void setOnOffs(std::vector<bool> ons);
    void setOnOffs(bool on);

    void changeTrig(bool test);
    // void updateGateCA();

 private:
    static AdeptDepp usb;
    unsigned deadtime_;
    unsigned pulse_delay_;
    int switches;
    int count;
    int counts[8];
    bool gate_ca[8];
    bool trg_none, trg_pulser, trg_beam, trg_aux, led_en;
    bool boards[8];
};

#endif  // __TRIGGER_HPP__
