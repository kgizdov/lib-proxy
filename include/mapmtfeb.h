/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/


#ifndef INCLUDE_MAPMTFEB_H_
#define INCLUDE_MAPMTFEB_H_

union mac_u {
  unsigned char uc[6];
  unsigned short us[3];
};

union ip_u {
  unsigned char uc[4];
  unsigned short us[2];
  unsigned int ui;
};

struct egress_ctl_t {
  unsigned short mep_static_destination :1;
  unsigned short mep_no_tfc :1;
  unsigned short tfc_throttle_0 :1;
  unsigned short tfc_throttle_1 :1;
  unsigned short tfc_throttle_0_polarity :1;
  unsigned short tfc_throttle_1_polarity :1;
  unsigned short tfc_decoder_reset_b :1;
  unsigned short gbe_poll_enable :1;
  unsigned short gbe_port :2;
  unsigned short gbe_static_port :1;
  unsigned short :5;
};

struct fe_ctl_t {
  unsigned short strobe_length :3;
  unsigned short skip_smb :1;
  unsigned short invert_fe :1;
  unsigned short enable_miim :1;
  unsigned short :1;
  unsigned short :1;
  unsigned short sources_complement :8;
  unsigned short sources_enable :8;
  unsigned short sources_trigger:8;
};

struct gt_ctl_t {
  unsigned short loopback_mode :3;
  unsigned short :13;
};

struct resets_t {
  unsigned short tfc :1;
  unsigned short dcms :1;
  unsigned short gtp :1;
  unsigned short daq :1;
  unsigned short gate :1;
  unsigned short dummy_ingress :1;
  unsigned short :10;
};

struct maroc_ctl_t {
  unsigned short mar1_cmuxrpt :1;  // Formerly mar1_cmuxen
  unsigned short mar2_cmuxrpt :1;  // Formerly mar2_cmuxen
  unsigned short :1;               // Formerly mar1_ck40en
  unsigned short :1;               // Formerly mar2_ck40en
  unsigned short mar1_skip_cfg :1;
  unsigned short mar2_skip_cfg :1;
  unsigned short mar1_adc_enable :1;
  unsigned short mar2_adc_enable :1;
  unsigned short cmux_stop :8;
  unsigned short hold_delay :8;
  unsigned short unused:8;
};

struct claro_ctl_t {
  unsigned short ec_version :1;
  unsigned short :15;
  unsigned short :16;
};

// The back-board (TB2014 version) settings

struct bb_ctl_t {
unsigned short bb_cs_smb_address :8;   // CLARO select base address
unsigned short bb_dac_smb_addr   :8;   // DAC base address
unsigned short bb_dac_value      :15;  // DAC value
unsigned short tpen              :1;   // Testpulse enable
};

struct febmap_t {
  mac_u macdst;                     // 0
  mac_u macsrc;                     // 6
  ip_u ipdst;                       // 12
  ip_u ipsrc;                       // 16
  unsigned short ip_protocol;       // 20
  unsigned short unused5;           // 22 Formerly ip_tos
  unsigned short ip_ttl;            // 24
  unsigned short unused6;           // 26 Formerly ip_type;
  unsigned short unused7;           // 28 Formerly ip_version;
  egress_ctl_t egress_ctl;          // 30
  unsigned tag;                     // 32 Formerly partition
  unsigned short mep_event_count;   // 36
  unsigned short unused1;           // 38
  gt_ctl_t gt_ctl;                  // 40
  resets_t resets;                  // 42
  unsigned short trigger_delay;     // 44
  unsigned short l1_id;             // 46
  fe_ctl_t fe_ctl;                  // 48
  unsigned pulse_count;             // 52
  unsigned short latency;           // 56
  unsigned short dummy_size;        // 58 Formerly MTU
  unsigned short mep_hwm;           // 60
  unsigned short pulse_delay;       // 62
};

struct ttcBData_t {
  unsigned subaddr_data :16;
  unsigned high :1;
  unsigned external :1;
  unsigned ttcrx_addr :14;
};

#endif  // INCLUDE_MAPMTFEB_H_
