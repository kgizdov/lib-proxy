// Copyright 2016 Konstantin Gizdov University of Edinburgh

#ifndef __RICH_CONTROL_HPP__
#define __RICH_CONTROL_HPP__

#include <iostream>
#include <string>
#include <vector>

#include "./tinyxml2.h"
#include "./xml_helpers.hpp"
#include "./common_prefs.hpp"
#include "./custom.hpp"
#include "./mapmtfeb.hpp"
#include "./maroc3.hpp"
#include "./dna.hpp"
#include "./trigger.hpp"
// #include "claro.hpp"  // LEFT FOR LATER

extern Dna dnaStore;
extern TriggerBoard trigB;

// extern tinyxml2::XMLDocument settings, prefsdoc, chimDNA, claroPD, marocPD, marocPDnew, rich_pckd, richXY, labels;

// extern commonPrefsStruct commonPrefs;

extern uint32_t proxyMsgNum, runNumber;  // keep track of message instance and run number

extern int inchannel, outchannel;  // input and output for stdIO communication with proxy
extern int trig_count;  // status variables

extern bool recording;  // are we recording or not

extern MAROC3 mar;
extern MAPMTFEB map;

extern int threshold;
extern bool stop, is_running, parent;
extern pthread_mutex_t wait_talk, wait_count, wait_usb, wait_thread, wait_stop, wait_fifo, wait_threshold, wait_parent;

// extern std::vector<dna_t> dnas;

void *trigCountFIFO(void * fifo_file);

void *checkCounter(void *trigB);

void flipBuffer(byte inbuf[], int size);

void writeIntToBytes(int32_t inputInt, byte byteArray[], int offset);

void writeShortToBytes(int16_t inputInt, byte byteArray[], int offset);

void writeUIntToBytes(int32_t inputInt, byte byteArray[], int offset);

void writeUShortToBytes(int16_t inputInt, byte byteArray[], int offset);

void writeToParent(msg_u msg, const std::string &message, unsigned is_err = 0);

void readProxyResponse();

void sendHeader(uint16_t length, uint16_t type, uint32_t info, bool wait);

void sendConfiguration(std::string dna, int maxEvents, bool wait);

void SetupProxy(bool custom, const commonPrefsStruct& commonPrefs, Dna dnaStore);

int NewRun();

void StartRun(bool custom, const commonPrefsStruct& commonPrefs,
              tinyxml2::XMLDocument *settings, tinyxml2::XMLDocument *marocPD, Dna dnaStore);

void EndRun(bool custom, const commonPrefsStruct& commonPrefs,
            tinyxml2::XMLDocument *settings, tinyxml2::XMLDocument *marocPD, Dna dnaStore);

void LaunchProxy();

int readCommonPrefs(tinyxml2::XMLDocument *doc, std::string docName, commonPrefsStruct* commonPrefs);

int saveCommonPrefs(tinyxml2::XMLDocument *doc, std::string docName, const commonPrefsStruct &commonPrefs);

int iterateRun(tinyxml2::XMLDocument *doc, std::string docName);

int storeRunSettings(tinyxml2::XMLDocument *prefsdoc, tinyxml2::XMLDocument *settings,
                     tinyxml2::XMLDocument *marocPD, Dna dnaStore);

#endif  // __RICH_CONTROL_HPP__
