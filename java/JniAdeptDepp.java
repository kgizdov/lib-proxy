// API for Digilent Adept Depp interface

package cbsw.instrument.adept;

import java.awt.event.*;
import javax.swing.Timer;
import java.util.concurrent.locks.*;

public class JniAdeptDepp {

    static {
        System.loadLibrary("JniAdeptDepp");
    }

    public class DeppException extends Exception {
        public DeppException(){;}
    };

    private Timer timer;
    private boolean isOK = false;
    private String deviceString;

    ActionListener timerAL = new ActionListener () {
        public void actionPerformed(ActionEvent e) {
            // Open the device if not already open

            try
            {
                if ( !isOK ) close();
                if ( !isOK ) open();
            }
            catch ( DeppException de ){System.out.println("JniAdeptDepp.timerAL(): Exception opening "+deviceString);}

            //  if ( !isOK ) System.out.println("Waiting for "+deviceString);

            return;
        }
    };

    ReentrantLock lock;

    public JniAdeptDepp( String dev ) {
        deviceString = dev;

        lock = new ReentrantLock();

        timer = new Timer(1000, timerAL );
        timer.setInitialDelay(0);
        timer.setCoalesce( true );
    }

    public JniAdeptDepp( String dev, ActionListener al ) {
        deviceString = dev;

        lock = new ReentrantLock();

        timer = new Timer(1000, al );
        timer.setInitialDelay(0);
        timer.setCoalesce( true );
    }

    public static native int enumerate();
    public static native String getEnumeratedDevice( int idx );
    public native int deppOpen( String dev );
    public native int deppClose();
    public native int deppGetSet( byte [] addr, byte [] data );
    public native int deppGetRepeat( byte addr, byte [] buf );
    public native int deppPutSet( byte [] buf );
    public native int deppPutRepeat( byte addr, byte [] buf );

    public boolean OK() {
        return isOK;
    }

    public void connect() {
        timer.start();
        return;
    }

    public void disconnect() {
        timer.stop();
        close();
        return;
    }

    public void open() throws DeppException {
        isOK = false;
        //        System.out.println("Open "+deviceString);
        try {
            lock.lock();
            int rc = deppOpen( deviceString );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    public void close() {
        isOK = false;
        try {
            lock.lock();
            int rc = deppClose();
        }
        finally {
            lock.unlock();
        }

        return;
    }

    // Get single byte

    public void get( byte addr, byte [] buf ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            byte [] addrArray = new byte[1]; addrArray[0] = addr;
            int rc = deppGetSet( addrArray, buf );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    // Get multiple data

    public void get( byte [] addr, byte [] data ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            int rc = deppGetSet( addr, data );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    // Put single addr,byte

    public void put( byte addr, byte data ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            byte [] buf = new byte[2]; buf[0] = addr; buf[1] = data;
            int rc = deppPutSet( buf );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    // Buffer contains addr,data pairs

    public void put( byte [] buf ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            int rc = deppPutSet( buf );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    // Push buf[0],buf[1],...,buf[buf.legth-1] to addr

    public void push( byte addr, byte [] buf ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            int rc = deppPutRepeat( addr, buf );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }

    // Pop buf[0],buf[1],...,buf[buf.legth-1] from addr

    public void pop( byte addr, byte [] buf ) throws DeppException {
        if ( !isOK ) throw new DeppException();

        try {
            lock.lock();
            int rc = deppGetRepeat( addr, buf );
            isOK = (rc == 1);
        }
        finally {
            lock.unlock();
            if ( !isOK ) throw new DeppException();
        }

        return;
    }
}
