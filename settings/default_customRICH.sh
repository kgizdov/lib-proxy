#!/bin/bash

# Change dir to the current folder
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Change dir manually to the current folder
# cd /

# The following line redirects error messages. Can be changed.
# Do not redirect stdin or stdout. They are used for communication with the GUI
exec 2>rich_control.log

# The following line redirects all output. Can be changed.
# THIS IS FOR DEBUGGIN PURPOSES ONLY
# exec &>rich_control.log

# Check installed dependencies
check=0
if [[ $(ldconfig -p | grep libdabs > /dev/null || ls | grep libdabs > /dev/null ; echo $?) -ne 0 ]]; then
    echo "ERROR: missing dependency -> libdabs (maybe also libdepp, libdftd2xx, libdmgr & libdpcomm)" >&2
    check=1
elif [[ $(ldconfig -p | grep libdepp > /dev/null || ls | grep libdepp > /dev/null ; echo $?) -ne 0 ]]; then
    echo "ERROR: missing dependency -> libdepp (maybe also libdftd2xx, libdmgr & libdpcomm)" >&2
    check=1
elif [[ $(ldconfig -p | grep libdftd2xx > /dev/null || ls | grep libdftd2xx > /dev/null ; echo $?) -ne 0 ]]; then
    echo "ERROR: missing dependency -> libdftd2xx (maybe also libdmgr & libdpcomm)" >&2
    check=1
elif [[ $(ldconfig -p | grep libdmgr > /dev/null || ls | grep libdmgr > /dev/null ; echo $?) -ne 0 ]]; then
    echo "ERROR: missing dependency -> libdmgr (maybe also libdmgr & libdpcomm)" >&2
    check=1
elif [[ $(ldconfig -p | grep libdpcomm > /dev/null || ls | grep libdpcomm > /dev/null ; echo $?) -ne 0 ]]; then
    echo "ERROR: missing dependency -> libdpcomm" >&2
    check=1
fi

if [[ $USER -eq "gizdov" ]]; then
    check=0
fi

if [[ check -eq 1 ]]; then
    unset check
    echo "Please make sure Digilent Adept libraries are installed and configured!" >&2
    echo "Dependencies not satisfied. Exiting..." >&2
    echo "ml"  # signal missing libraries error
    exit 1
fi

# FEB_PROXY_KEYFILE=/tmp/feb-proxy-keyfile
# touch $FEB_PROXY_KEYFILE
# sudo ./feb-proxy $@ --keyfile $FEB_PROXY_KEYFILE
sudo ./bin/rich_control

# echo "x"
echo "rich_control done." >&2
