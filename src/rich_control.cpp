// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#include "../include/rich_control.hpp"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"
#include "../include/common_prefs.hpp"
#include "../include/custom.hpp"
#include "../include/mapmtfeb.hpp"
#include "../include/maroc3.hpp"
#include "../include/trigger.hpp"

// #include "../include/claro.hpp"  // LEFT FOR LATER

Dna dnaStore;

commonPrefsStruct commonPrefs;

uint32_t proxyMsgNum = 0, runNumber;  // keep track of message instance and run number

int inchannel, outchannel;  // input and output for stdIO communication with proxy
int trig_count = 0;  // status variables

bool recording = true;  // assume recording

MAROC3 mar;
MAPMTFEB map;

int threshold = 0;
bool stop = false, is_running = false, parent = true;
pthread_mutex_t wait_talk = PTHREAD_MUTEX_INITIALIZER, wait_count = PTHREAD_MUTEX_INITIALIZER,
                wait_usb = PTHREAD_MUTEX_INITIALIZER, wait_thread = PTHREAD_MUTEX_INITIALIZER,
                wait_stop = PTHREAD_MUTEX_INITIALIZER, wait_fifo = PTHREAD_MUTEX_INITIALIZER,
                wait_threshold = PTHREAD_MUTEX_INITIALIZER, wait_parent = PTHREAD_MUTEX_INITIALIZER;

void *trigCountFIFO(void *fifo_file) {
    // fifo_file is if int type
    int * fifo_p = reinterpret_cast<int*>(fifo_file);
    int fifo = *fifo_p;
    if (fifo < 3) {  // fifo needs to be larger than STDIO
        pthread_exit(NULL);
        return NULL;
    }
    std::stringstream str_count;
    pthread_mutex_lock(&wait_parent);
    bool parent_on = parent;
    pthread_mutex_unlock(&wait_parent);
    while (parent_on) {
        pthread_mutex_lock(&wait_parent);
        parent_on = parent;
        pthread_mutex_unlock(&wait_parent);
        if (!parent_on) {
            break;
        }
        bool run;
        pthread_mutex_lock(&wait_thread);
        run = is_running;
        pthread_mutex_unlock(&wait_thread);
        if (!run) {
            sleep(1);
            continue;
        }
        pthread_mutex_lock(&wait_count);
        str_count << trig_count;
        pthread_mutex_unlock(&wait_count);
        pthread_mutex_lock(&wait_fifo);
        write(fifo, str_count.str().c_str(), sizeof(str_count.str().c_str()));
        pthread_mutex_unlock(&wait_fifo);
        str_count.str(std::string());  // reset string count

        // check for quit multiple times
        pthread_mutex_lock(&wait_parent);
        parent_on = parent;
        pthread_mutex_unlock(&wait_parent);
        if (!parent_on) {
            break;
        }
        sleep(1);
    }
    pthread_exit(NULL);
    return NULL;
}

void *checkCounter(void *trigB) {
    bool do_stop = false;
    pthread_mutex_lock(&wait_thread);
    is_running = true;
    pthread_mutex_unlock(&wait_thread);
    // reset counts on start
    pthread_mutex_lock(&wait_usb);
    reinterpret_cast<TriggerBoard*>(trigB)->resetCounts();
    pthread_mutex_unlock(&wait_usb);
    pthread_mutex_lock(&wait_count);
    trig_count = 0;
    pthread_mutex_unlock(&wait_count);
    while (!do_stop) {
        pthread_mutex_lock(&wait_stop);
        do_stop = stop;
        pthread_mutex_unlock(&wait_stop);
        if (do_stop) {
            break;
        }
        int check = 0;
        pthread_mutex_lock(&wait_threshold);
        check = threshold;
        pthread_mutex_unlock(&wait_threshold);
        if (check <= 100) {  // safety margin
            sleep(1);
            continue;
        }

        int usb_count;
        pthread_mutex_lock(&wait_usb);
        reinterpret_cast<TriggerBoard*>(trigB)->updateCount();
        reinterpret_cast<TriggerBoard*>(trigB)->updateGateCA();
        usb_count = reinterpret_cast<TriggerBoard*>(trigB)->getCount();
        std::cerr << "INFO: Trigger count is " << usb_count << std::endl;
        pthread_mutex_unlock(&wait_usb);

        pthread_mutex_lock(&wait_count);
        trig_count = usb_count;
        pthread_mutex_unlock(&wait_count);

        if (usb_count >= check) {
            // KILL ACQUISITION - EndRun()
            pthread_mutex_lock(&wait_talk);
            EndRun(false, commonPrefs, &settings, &marocPD, dnaStore);
            pthread_mutex_unlock(&wait_talk);

            pthread_mutex_lock(&wait_stop);
            do_stop = true;
            pthread_mutex_unlock(&wait_stop);

            // reset counts
            pthread_mutex_lock(&wait_usb);
            reinterpret_cast<TriggerBoard*>(trigB)->resetCounts();
            pthread_mutex_unlock(&wait_usb);

            // sendHeader((uint16_t)0, (uint16_t)3, 0, true);  // send end run signal
        }
        if (do_stop) {  // try to check multiple times for an update
            break;
        }
        sleep(1);
    }
    pthread_mutex_lock(&wait_thread);
    is_running = false;
    pthread_mutex_unlock(&wait_thread);
    pthread_exit(NULL);
    return NULL;
}

void flipBuffer(byte inbuf[], unsigned size) {
    byte * temp;
    temp = static_cast<byte*>(malloc(size * sizeof(byte)));
    if (temp == NULL) {
        std::cerr << "ERROR: Could not allocate memory for flipping buffer... Aborting." << std::endl;
        return;
    }
    memcpy(temp, inbuf, size);
    for (unsigned i = 0; i < size; i++) {
        inbuf[i] = temp[size-i];
    }
    free(temp);
    return;
}

void writeIntToBytes(int32_t inputInt, byte byteArray[], int offset) {
    byteint temp;
    temp.i = inputInt;
    for (int i = 0; i < 4; i++) {
        byteArray[i+offset] = temp.b[i];
    }
    return;
}

void writeShortToBytes(int16_t inputInt, byte byteArray[], int offset) {
    byteshort temp;
    temp.i = inputInt;
    for (int i = 0; i < 2; i++) {
        byteArray[i+offset] = temp.b[i];
    }
    return;
}

void writeUIntToBytes(uint32_t inputUInt, byte byteArray[], int offset) {
    byteuint temp;
    temp.i = inputUInt;
    for (int i = 0; i < 4; i++) {
        byteArray[i+offset] = temp.b[i];
    }
    return;
}

void writeUShortToBytes(uint16_t inputUShort, byte byteArray[], int offset) {
    byteushort temp;
    temp.i = inputUShort;
    for (int i = 0; i < 2; i++) {
        byteArray[i+offset] = temp.b[i];
    }
    return;
}

void writeStringToBytes(const std::string &input, byte byteArray[], int offset) {
    for (unsigned i = 0; i < input.length(); ++i) {
        byteArray[i+offset] = input[i];
    }
    return;
}

void bufferToMessage(byte in_buffer[], byte proxy_Msg[]) {
    for (unsigned i = 0; i < kBufSendSize; ++i) {
        proxy_Msg[i] = in_buffer[i];
    }
    return;
}

void writeToParent(msg_u msg, const std::string &message, unsigned is_err) {
    bool debug = true;
    if (message.compare("ok") == 0 || message.compare("er") == 0) {
        msg.inf.length = 0;
    } else {
        msg.inf.length = message.size();
    }
    msg.inf.comm = 0;
    msg.inf.info = is_err;
    // msg.inf.seq is already read from parent

    if (write(1, &msg.b, sizeof(msg.b)) != sizeof(msg.b)) {
        std::cerr << "ERROR: Could not respond to parent..." << std::endl;
        msg.inf.length = 0;  // nulify command
    }
    fflush(stdout);
    if (debug) {
        std::cerr << "DEBUG: command to PARENT [length, command, info, sequence] -> "
        << msg.inf.length << " " << msg.inf.comm << " " << msg.inf.info << " " << msg.inf.seq << std::endl;
    }
    if (msg.inf.length > 0) {
        write(1, message.c_str(), message.size());
        fflush(stdout);
        std::cerr << "[SENT TO PARENT] -> " << message.c_str() << std::endl;
    }
    return;
}

void readProxyResponse() {
    byteuint b_uint;
    uint32_t i = 0;
    while (i < proxyMsgNum) {
        for (unsigned j = 0; j < 4; j++) {
            if (read(inchannel, (&b_uint.b[j]), 1) < 0) {
                std::cerr << "ERROR: cannot read from pipe... Aborting." << std::endl;
                return;
            }
        }
        i = b_uint.i;
    }
    std::cerr << "Read response number " << i << std::endl;
}

void sendHeader(uint16_t length, uint16_t type, uint32_t info, bool wait) {
    // std::vector<byte> proxyMsg(proxyMsgSize);  // not sure if I want a vector for now
    byte proxyMsg[12];
    proxyMsgNum++;

    // msg.hdr.[type, length, info, seq] = [4,6,0,1] = config
    // msg.hdr.[type, length, info, seq] = [1,388,0,2] = start/stop
    // msg.hdr.[type, length, info, seq] = [3,0,0,2] = end
    //

    writeUShortToBytes(length, proxyMsg, 0);
    writeUShortToBytes(type, proxyMsg, 2);
    writeUIntToBytes(info, proxyMsg, 4);
    writeUIntToBytes(proxyMsgNum, proxyMsg, 8);
    // SEND MESSAGE
    // write(outchannel, proxyMsg, proxyMsgSize);
    if (write(outchannel, proxyMsg, sizeof(proxyMsg)) != sizeof(proxyMsg)) {  // header would never be bigger than 12 bytes
        std::cerr << "ERROR: sendHeader() did not write all bytes... Aborting." << std::endl;
    }
    fflush(stdout);
    std::cerr << "sent header [type, length, info, seq]: "
    << type << " " << length << " " << info << " " << proxyMsgNum << std::endl;
    if (wait) readProxyResponse();  // wait for response
    return;
}

void sendConfiguration(std::string dna, int maxEvents, bool wait) {
    byte buffer[kBufSendSize];
    byte proxyMsg[kProxyMsgSize];

    // send Header
    sendHeader((uint16_t)kBufSendSize, (uint16_t)1, maxEvents, false);

    // Board DNA
    buffer[3] = strtoul(dna.substr(0, 2).c_str(), 0, 16);
    buffer[2] = strtoul(dna.substr(2, 2).c_str(), 0, 16);
    buffer[1] = strtoul(dna.substr(5, 2).c_str(), 0, 16);
    buffer[0] = strtoul(dna.substr(7, 2).c_str(), 0, 16);

    map.getRegisterArray(buffer, 72, 4);  // TO BE IMPLEMENTED
    for (unsigned i = 0; i < 2; i++) mar.getRegisterArray(i, buffer, 104, 128+4+128*i);

    // put buffer in message
    proxyMsgNum++;
    // memcpy(proxyMsg, buffer, kbufSendSize);

    // SEND MESSAGE
    // write(outchannel, proxyMsg, kproxyMsgSize);
    bufferToMessage(buffer, proxyMsg);

    if (write(outchannel, proxyMsg, kBufSendSize) != kBufSendSize) {
        std::cerr << "ERROR: sendConfiguration() did not write all bytes... Aborting." << std::endl;
    }
    fflush(stdout);
    std::cerr << "sent config" << std::endl;
    if (wait) readProxyResponse();  // wait for response
    return;
}

void SetupProxy(bool custom, const commonPrefsStruct &commonPrefs, Dna dnaStore) {
    std::vector<dna_t> dnas = dnaStore.getDNAs();
    if (dnas.size() == 0) {
        std::cerr << "ERROR: No Boards configured. Cannot start acquisition... Aborting." << std::endl;
        return;
    }
    char cwd[FILENAME_MAX];
    std::vector<std::string> filenames;
    std::vector<std::string> macAddresses;
    std::vector<std::string> ipAddresses;
    std::string sb;
    byte proxyMsg[kProxyMsgSize];

    if (getcwd(cwd, sizeof(cwd)) == NULL) {
        std::cerr << "ERROR: Could not get current working directory... Aborting." << std::endl;
        return;
    }

    std::string folder = commonPrefs.runtime.saveFileDestination;
    // folder = "./";
    // if (cwd != folder.c_str()) {
    if (!custom) {
        folder = cwd;
        folder.append("/");
    }

    bool logging = false;
    if (logging) {  // log datetime into folder name
        time_t t = time(0);
        struct tm now_r;
        localtime_r(&t, &now_r);
        std::ostringstream time;
        time << (now_r.tm_year + 1900) << '-'
             << (now_r.tm_mon + 1) << '-'
             <<  now_r.tm_mday << '-'
             <<  now_r.tm_hour << ':'
             <<  now_r.tm_min;
        folder += time.str().c_str();
        std::string command = "mkdir -p ";
        command += folder;
        // std::cerr << system(command.c_str()) << std::endl;
        system(command.c_str());
        folder.append("/");
    }

    dnaStore.setCustomFolderNames(folder);  // store folder for later reference;

    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].on == 0) {
            continue;
        }
        if (runNumber <= 999999) {
            filenames.push_back(folder + "feb-" + dnas[i].file + "-%06d.mdf");
        } else {
            uint32_t fix = runNumber / 1000000 % 10;  // compensate for filename size
            filenames.push_back(folder + "feb-" + dnas[i].file + "-" + fix + "%06d.mdf");
        }
        macAddresses.push_back(dnas[i].mac);  // dummy
        ipAddresses.push_back(dnas[i].ip);  // dummy
    }
    if (filenames.size() != macAddresses.size() || filenames.size() != ipAddresses.size()) {
        std::cerr << "ERROR: IP/MAC Address number mismatch...\n" << std::endl;
    }

    for (unsigned i = 0; i < filenames.size(); ++i) {
        sb.append(macAddresses[i]); sb.append("\0", 1);  // fix this (char)0
        sb.append(ipAddresses[i]); sb.append("\0", 1);
        sb.append(filenames[i]); sb.append("\0", 1);
    }

    sb.append("--"); sb.append("\0", 1);
    // pbw is always off, keep for compatibility
    sb.append("--"); sb.append("\0", 1);

    std::cerr << sb << std::endl;

    sendHeader((uint16_t)sb.length(), (uint16_t)4, 0, false);

    writeStringToBytes(sb, proxyMsg, 0);

    proxyMsgNum++;

    // SEND MESSAGE
    // write(outchannel, proxyMsg, proxyMsgSize);
    if (write(outchannel, proxyMsg, sb.length()) != static_cast<int>(sb.length())) {
        std::cerr << "ERROR: SetupProxy() did not write all bytes... Aborting." << std::endl;
    }
    fflush(stdout);
    // readProxyResponse();  // this should be disabled for now
}

int NewRun() {
    // // Export preferences
    // exportPreferences();

    // increment only if recording
    if (!recording) {
        return runNumber;
    }
    runNumber++;
    // Send the new run message
    sendHeader((uint16_t)0, (uint16_t)2, runNumber, true);  // tell proxy new run number

    return runNumber;
}

void StartRun(bool custom, const commonPrefsStruct &commonPrefs,
    tinyxml2::XMLDocument *settings, tinyxml2::XMLDocument *marocPD, Dna dnaStore) {

    std::vector<dna_t> dnas = dnaStore.getDNAs();
    if (dnas.size() == 0) {
        return;
    }

    pthread_mutex_lock(&wait_stop);
    stop = false;  // start thread
    pthread_mutex_unlock(&wait_stop);
    NewRun();

    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].on == 0) {
            continue;
        }
        map.setStateFromPreferences(commonPrefs, dnas[i]);
        map.setDaqReset(false);
        map.setDummyIngressReset(true);
        map.setGate(true);
        map.setTag(0);
        map.setMepTfcControl(false);
        map.setMepFixedDestination(true);
        map.setTfcDecoderEnable(true);
        map.setGbePollEnable(true);

        mar.setStateFromPreferences(settings, marocPD, dnas[i].db);
        if (custom) {
            map.changeTrigger(dnas[i].trig);
            map.setTestPulseEnable(dnas[i].testpulse);
            map.setDACval(dnas[i].dac);
            map.setMarHoldDelay(dnas[i].hold);
            mar.setGlobalGain(marocPD, dnas[i].pmt, dnas[i].db);
            mar.setGlobalCTest(marocPD, dnas[i].testpulse, dnas[i].db);
        }

        sendConfiguration(dnas[i].name, 0, false);
    }

    return;
}

void EndRun(bool custom, const commonPrefsStruct &commonPrefs,
    tinyxml2::XMLDocument *settings, tinyxml2::XMLDocument *marocPD, Dna dnaStore) {
    std::vector<dna_t> dnas = dnaStore.getDNAs();

    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].on == 0) {
            continue;
        }
        map.setStateFromPreferences(commonPrefs, dnas[i]);
        map.setDaqReset(true);
        map.setGate(false);
        map.setTag(0);
        map.setMepTfcControl(false);
        map.setMepFixedDestination(true);
        map.setTfcDecoderEnable(false);
        map.setGbePollEnable(false);

        mar.setStateFromPreferences(settings, marocPD, dnas[i].db);
        if (custom) {
            map.changeTrigger(dnas[i].trig);
            map.setTestPulseEnable(dnas[i].testpulse);
            map.setDACval(dnas[i].dac);
            map.setMarHoldDelay(dnas[i].hold);
            mar.setGlobalGain(marocPD, dnas[i].pmt, dnas[i].db);
            mar.setGlobalCTest(marocPD, dnas[i].testpulse, dnas[i].db);
        }

        sendConfiguration(dnas[i].name, 0, false);
    }

    sendHeader((uint16_t)0, (uint16_t)3, 0, true);  // send end run signal
    pthread_mutex_lock(&wait_stop);
    stop = true;  // stop thread
    pthread_mutex_unlock(&wait_stop);

    return;
}


void LaunchProxy() {
}

int readCommonPrefs(tinyxml2::XMLDocument *doc, std::string docName, commonPrefsStruct *commonPrefs) {
    tinyxml2::XMLError errCheck = doc->LoadFile(docName.c_str());
    if (errCheck != 0) {
        XMLCheckSuccess(errCheck);
        return -1;
    }

    // // lookupXMLValue();  // not sure if needed
    getStingPropertyFromXML2(doc, "proxy.path", &(commonPrefs[0].proxy.path), "./scripts");
    getBoolPropertyFromXML2(doc, "proxy.pbw.enable", &(commonPrefs->proxy.pbw.enable), "false");
    getStingPropertyFromXML2(doc, "proxy.pbw.fifo", &(commonPrefs[0].proxy.pbw.fifo), "/dev/shm/pbwrly");
    getBoolPropertyFromXML2(doc, "proxy.trig.enable", &(commonPrefs->proxy.trig.enable), "true");
    getStingPropertyFromXML2(doc, "proxy.trig.fifo", &(commonPrefs[0].proxy.trig.fifo), "/dev/shm/trig_count_fifo");
    getStingPropertyFromXML2(doc, "runtime.feb-type", &(commonPrefs[0].runtime.feb_type), "MAROC3");
    getIntPropertyFromXML2(doc, "runtime.latency", &(commonPrefs->runtime.latency), 16);
    getIntPropertyFromXML2(doc, "runtime.maxEvents", &(commonPrefs->runtime.maxEvents), 1);
    getIntPropertyFromXML2(doc, "runtime.pulseCount", &(commonPrefs->runtime.pulseCount), 100000);
    getIntPropertyFromXML2(doc, "runtime.pulseDelay", &(commonPrefs->runtime.pulseDelay), 3500);
    getBoolPropertyFromXML2(doc, "runtime.recording", &(commonPrefs->runtime.recording), true);
    getUnsignedPropertyFromXML2(doc, "runtime.runNumber", &(commonPrefs->runtime.runNumber), 1);
    getStingPropertyFromXML2(doc, "runtime.saveFileDestination", &(commonPrefs[0].runtime.saveFileDestination), "/home/lhcbuser/lhcb/RICHECKIT/data/");
    getBoolPropertyFromXML2(doc, "runtime.saveRunPreferences", &(commonPrefs->runtime.saveRunPreferences), true);
    getIntPropertyFromXML2(doc, "runtime.strobeLength", &(commonPrefs->runtime.strobeLength), 5);
    getIntPropertyFromXML2(doc, "runtime.tag", &(commonPrefs->runtime.tag), 0);
    getIntPropertyFromXML2(doc, "runtime.triggerDelay", &(commonPrefs->runtime.triggerDelay), 20);
    getBoolPropertyFromXML2(doc, "runtime.triggerPanel", &(commonPrefs->runtime.triggerPanel), true);
    getIntPropertyFromXML2(doc, "scan.begin", &(commonPrefs->scan.begin), 0);
    getIntPropertyFromXML2(doc, "scan.end", &(commonPrefs->scan.end), 63);
    getIntPropertyFromXML2(doc, "scan.maxEvents", &(commonPrefs->scan.maxEvents), 100);
    getIntPropertyFromXML2(doc, "scan.step", &(commonPrefs->scan.step), 2);

    int debug = 0;
    if (debug) {
        std::cerr << "proxy path: " << commonPrefs->proxy.path << std::endl;
        std::cerr << "proxy pbw enable: " << commonPrefs->proxy.pbw.enable << std::endl;
        std::cerr << "proxy pbw fifo: " << commonPrefs->proxy.pbw.fifo << std::endl;
        std::cerr << "runtime.feb_type: " << commonPrefs->runtime.feb_type << std::endl;
        std::cerr << "runtime latency: " << commonPrefs->runtime.latency << std::endl;
        std::cerr << "runtime.maxEvents: " << commonPrefs->runtime.maxEvents << std::endl;
        std::cerr << "runtime.pulseCount: " << commonPrefs->runtime.pulseCount << std::endl;
        std::cerr << "runtime.pulseDelay: " << commonPrefs->runtime.pulseDelay << std::endl;
        std::cerr << "runtime.recording: " << commonPrefs->runtime.recording << std::endl;
        std::cerr << "runtime.runNumber: " << commonPrefs->runtime.runNumber << std::endl;
        std::cerr << "runtime.saveFileDestination: " << commonPrefs->runtime.saveFileDestination << std::endl;
        std::cerr << "runtime.saveRunPreferences: " << commonPrefs->runtime.saveRunPreferences << std::endl;
        std::cerr << "runtime.strobeLength: " << commonPrefs->runtime.strobeLength << std::endl;
        std::cerr << "runtime.tag: " << commonPrefs->runtime.tag << std::endl;
        std::cerr << "runtime.triggerDelay: " << commonPrefs->runtime.triggerDelay << std::endl;
        std::cerr << "runtime.triggerPanel: " << commonPrefs->runtime.triggerPanel << std::endl;
        std::cerr << "scan.begin: " << commonPrefs->scan.begin << std::endl;
        std::cerr << "scan.end: " << commonPrefs->scan.end << std::endl;
        std::cerr << "scan.maxEvents: " << commonPrefs->scan.maxEvents << std::endl;
        std::cerr << "scan.step: " << commonPrefs->scan.step << std::endl;
    }

    return 0;
}

int saveCommonPrefs(tinyxml2::XMLDocument *doc, std::string docName, const commonPrefsStruct &commonPrefs) {
    int check = 0;
    check += setBoolPropertyIntoXML(doc, "proxy.trig.enable", commonPrefs.proxy.trig.enable);
    check += setStringPropertyIntoXML(doc, "proxy.trig.fifo", commonPrefs.proxy.trig.fifo);
    check += setStringPropertyIntoXML(doc, "runtime.feb-type", commonPrefs.runtime.feb_type);
    check += setIntPropertyIntoXML(doc, "runtime.pulseCount", commonPrefs.runtime.pulseCount);
    check += setIntPropertyIntoXML(doc, "runtime.pulseDelay", commonPrefs.runtime.pulseDelay);
    check += setBoolPropertyIntoXML(doc, "runtime.recording", commonPrefs.runtime.recording);
    check += setIntPropertyIntoXML(doc, "runtime.runNumber", commonPrefs.runtime.runNumber);
    check += setStringPropertyIntoXML(doc, "runtime.saveFileDestination", commonPrefs.runtime.saveFileDestination);
    if (check != 0) {
        std::cerr << "ERROR: Could not fully export runtime settings" << std::endl;
    }
    doc->SaveFile(docName.c_str());
    return check;
}

int iterateRun(tinyxml2::XMLDocument *doc, std::string docName) {
    int check;
    check = setIntPropertyIntoXML(doc, "runtime.runNumber", runNumber);
    if (check != 0) {
        std::cerr << "ERROR: Could not save runNumber" << std::endl;
    }
    doc->SaveFile(docName.c_str());
    return check;
}

int storeRunSettings(tinyxml2::XMLDocument *prefsdoc,
    tinyxml2::XMLDocument *settings, tinyxml2::XMLDocument *marocPD, Dna dnaStore) {
    std::vector<dna_t> dnas = dnaStore.getDNAs();
    // make custom board XML file
    //

    // make runtime XML file
    //

    for (unsigned i = 0; i < dnas.size(); ++i) {
        std::string dummy_folder = dnas[i].folder + runNumber + "/log/";
        std::string command = "mkdir -p " + dummy_folder;
        system(command.c_str());
        std::string dummy_filename = dummy_folder + "mapping_" + dnas[i].file + ".xml";
        marocPD->SaveFile(dummy_filename.c_str());
        dummy_filename = dummy_folder + "general_settings_" + dnas[i].file + ".xml";
        settings->SaveFile(dummy_filename.c_str());
    }
    return 0;
}
