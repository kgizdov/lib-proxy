// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <string>
#include <iostream>

#include "../include/xml_helpers.hpp"

// tinyxml2::XMLDocument settings, prefsdoc, chimDNA, claroPD, marocPD, rich_pckd, richXY, labels;
tinyxml2::XMLDocument settings, prefsdoc, marocPD;

void lookupXMLValue() {}

tinyxml2::XMLDocument *newXMLDoc() {
    tinyxml2::XMLDocument *doc = new tinyxml2::XMLDocument();
    if (doc == NULL)
        return NULL;
    tinyxml2::XMLDeclaration *decl = doc->NewDeclaration();
    if (decl == NULL) {
        delete doc;
        return NULL;
    }
    if (decl != doc->InsertEndChild(decl)) {
        delete doc;
        return NULL;
    }
    return doc;
}

int addElementToDoc(tinyxml2::XMLDocument *doc, std::string name) {
    tinyxml2::XMLElement *elem = doc->NewElement(name.c_str());
    if (elem == NULL) {
        return -1;  // could not alloc mem
    }
    if (elem != doc->InsertEndChild(elem)) {
        return -2;  // could not insert elem
    }
    return 0;
}

int addCommentToDoc(tinyxml2::XMLDocument *doc, std::string comment) {
    tinyxml2::XMLComment *comt = doc->NewComment(comment.c_str());
    if (comt == NULL) {
        return -1;  // could not alloc mem
    }
    if (comt != doc->InsertEndChild(comt)) {
        return -2;  // could not insert elem
    }
    return 0;
}

int addElementToNode(tinyxml2::XMLNode *node, std::string name) {
    tinyxml2::XMLElement *elem = node->GetDocument()->NewElement(name.c_str());
    if (elem == NULL) {
        return -1;  // could not alloc mem
    }
    if (elem != node->InsertEndChild(elem)) {
        return -2;  // could not insert elem
    }
    return 0;
}

void setElementKeyAndValue(tinyxml2::XMLElement *elem, std::string keyName, std::string value) {
    elem->SetAttribute("key", keyName.c_str());
    elem->SetAttribute("value", value.c_str());
    return;
}

int setValueOfElementKey(tinyxml2::XMLElement *elem, std::string keyName, std::string value) {
    if (elem->Attribute("key") == NULL) {
        return -1;  // element has no property
    }
    if (elem->Attribute("key", keyName.c_str()) == NULL) {
        return -2;  // element has different property
    }
    elem->SetAttribute("value", value.c_str());
    return 0;
}

int moveToNode(tinyxml2::XMLNode *input_node, const std::string &name) {
    tinyxml2::XMLElement *elem = input_node->ToElement();
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(name)) {
            input_node = elem;
            return 0;
        }
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent() == input_node) {
                    return -1;
                }
                elem = elem->Parent()->ToElement();
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return -2;
}

tinyxml2::XMLNode *moveToNode2(tinyxml2::XMLNode *input_node, const std::string &name) {
    tinyxml2::XMLElement *elem = input_node->ToElement();
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(name)) {
            input_node = elem;
            return input_node;
        }
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent() == input_node) {
                    return NULL;
                }
                elem = elem->Parent()->ToElement();
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return NULL;
}

int moveToInnerNode(tinyxml2::XMLNode *input_node, const std::string &name) {
    tinyxml2::XMLElement *elem = input_node->ToElement();
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(name)) {
            input_node = elem;
            return 0;
        }
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent() == input_node) {
                    return -1;
                }
                elem = elem->Parent()->ToElement();
            }
            if (elem->Parent() == input_node) {
                return -1;
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return -2;
}

tinyxml2::XMLNode *moveToInnerNode2(tinyxml2::XMLNode *input_node, const std::string &name) {
    tinyxml2::XMLElement *elem = input_node->ToElement();
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(name)) {
            input_node = elem;
            return input_node;
            // return 0;
        }
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent() == input_node) {
                    return NULL;
                }
                elem = elem->Parent()->ToElement();
            }
            if (elem->Parent() == input_node) {
                return NULL;
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return NULL;
}

tinyxml2::XMLNode *getNodeByName(tinyxml2::XMLDocument *doc, const std::string &name) {
    tinyxml2::XMLNode *node = doc->RootElement();
    tinyxml2::XMLElement *elem = node->ToElement();
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(name)) return (elem);
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent()->ToElement() == doc->RootElement()) {
                    return (NULL);
                }
                elem = elem->Parent()->ToElement();
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return (NULL);
}

tinyxml2::XMLElement *getElementByName(tinyxml2::XMLDocument *doc, std::string elemName) {
    tinyxml2::XMLElement *elem = doc->RootElement();  // Tree root
    if (!std::string(elem->Name()).compare(elemName) || elem == NULL) {
        return elem;
    }
    while (elem) {
        if (!std::string(elem->Name()).compare(elemName)) return (elem);
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent()->ToElement() == doc->RootElement()) {
                    return (NULL);
                }
                elem = elem->Parent()->ToElement();
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return (NULL);
}

tinyxml2::XMLElement *getElementByNameFromNode(tinyxml2::XMLNode *input_node, std::string const &elemName) {
    tinyxml2::XMLElement *elem = input_node->ToElement();  // Tree root
    if (!std::string(elem->Name()).compare(elemName) || elem == NULL) {
        return elem;
    }
    while (elem) {
        if (!std::string(elem->Name()).compare(elemName)) return (elem);
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem == input_node->ToElement()) {
                    return (NULL);
                }
                elem = elem->Parent()->ToElement();
            }
            if (elem == input_node->ToElement()) {
                return (NULL);
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return (NULL);
}

tinyxml2::XMLElement *getElementByProperty(tinyxml2::XMLDocument *doc, std::string const &property_name) {
    tinyxml2::XMLElement *elem = doc->RootElement();  // Tree root
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(property_name)) return (elem);
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem->Parent()->ToElement() == doc->RootElement()) {
                    return (NULL);
                }
                elem = elem->Parent()->ToElement();
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return (NULL);
}

tinyxml2::XMLElement *getElementByPropertyFromNode(tinyxml2::XMLNode *input_node, std::string const &property_name) {
    tinyxml2::XMLElement *elem = input_node->ToElement();  // Tree root
    while (elem) {
        if (elem->FirstAttribute() && !std::string(elem->FirstAttribute()->Value()).compare(property_name)) return (elem);
        /*elem = elem->NextSiblingElement();*/
        if (elem->FirstChildElement()) {
            elem = elem->FirstChildElement();
        } else if (elem->NextSiblingElement()) {
            elem = elem->NextSiblingElement();
        } else {
            while (!elem->Parent()->NextSiblingElement()) {
                if (elem == input_node->ToElement()) {
                    return (NULL);
                }
                elem = elem->Parent()->ToElement();
            }
            if (elem == input_node->ToElement()) {
                return (NULL);
            }
            elem = elem->Parent()->NextSiblingElement();
        }
    }
    return (NULL);
}

int getIntPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, int &return_value, int default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryIntValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getIntPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, int *return_value, int default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryIntValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getUnsignedPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned &return_value, unsigned default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getUnsignedPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned *return_value, unsigned default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getBoolPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, bool &return_value, bool default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryBoolValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error bool parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getBoolPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, bool *return_value, bool default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryBoolValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error bool parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getStingPropertyFromXML(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string &return_value, std::string default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    // tinyxml2::XMLElement *elem = (NULL);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    return_value = std::string(elem->FirstAttribute()->Next()->Value());
    if (!return_value.empty()) {
        return 0;  // return success
    } else {
        // set default value and return error string parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getStingPropertyFromXML2(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string *return_value, std::string default_value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    // tinyxml2::XMLElement *elem = (NULL);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    return_value[0] = std::string(elem->FirstAttribute()->Next()->Value());
    if (!return_value[0].empty()) {
        return 0;  // return success
    } else {
        // set default value and return error string parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getIntPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, int &return_value, int default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryIntValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getIntPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, int *return_value, int default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryIntValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getUnsignedPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, unsigned &return_value, unsigned default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getUnsignedPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, unsigned *return_value, unsigned default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error int parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getBoolPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, bool &return_value, bool default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryBoolValue(&return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error bool parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getBoolPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, bool *return_value, bool default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    if (elem->FirstAttribute()->Next()->QueryBoolValue(return_value) == tinyxml2::XML_SUCCESS) {
        return 0;  // return success
    } else {
        // set default value and return error bool parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getStringPropertyFromXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, std::string &return_value, std::string default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    // tinyxml2::XMLElement *elem = (NULL);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value = default_value;
        return 0;
    }
    return_value = std::string(elem->FirstAttribute()->Next()->Value());
    if (!return_value.empty()) {
        return 0;  // return success
    } else {
        // set default value and return error string parsing
        return_value = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int getStringPropertyFromXMLNode2(tinyxml2::XMLNode *node, std::string const &property_name, std::string *return_value, std::string default_value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    // tinyxml2::XMLElement *elem = (NULL);
    if (!elem) {
        // if elem does not exist, return the default value
        return_value[0] = default_value;
        return 0;
    }
    return_value[0] = std::string(elem->FirstAttribute()->Next()->Value());
    if (!return_value[0].empty()) {
        return 0;  // return success
    } else {
        // set default value and return error string parsing
        return_value[0] = default_value;
        return -1;
    }
    return -2;  // return -2 for other errors; useful for debugging
}

int setIntPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, int value) {
    int check;
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryIntValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setIntPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, int value) {
    int check;
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryIntValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setUnsignedPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, unsigned value) {
    unsigned check;
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setUnsignedPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, unsigned value) {
    unsigned check;
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryUnsignedValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setBoolPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, bool value) {
    bool check;
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryBoolValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setBoolPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, bool value) {
    bool check;
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, it cannot be set
        return -1;
    }
    elem->SetAttribute("value", value);
    if (elem->FirstAttribute()->Next()->QueryBoolValue(&check) == tinyxml2::XML_SUCCESS) {
        if (check == value) {
            return 0;  // return success
        } else {
            return -2;  // set value is incorrect
        }
    } else {
        // value was not set
        return -3;
    }
    return -4;  // unknown error; useful for debugging
}

int setStringPropertyIntoXML(tinyxml2::XMLDocument *doc, std::string const &property_name, std::string value) {
    tinyxml2::XMLElement *elem = getElementByProperty(doc, property_name);
    if (!elem) {
        // if elem does not exist, so cannot be set
        return -1;
    }
    elem->SetAttribute("value", value.c_str());
    if (!std::string(elem->FirstAttribute()->Next()->Value()).compare(value)) {
        return 0;  // return success
    } else {
        // value after set is incorrect or missing
        return -1;
    }
    return -2;  // unknown error; useful for debugging
}

int setStringPropertyIntoXMLNode(tinyxml2::XMLNode *node, std::string const &property_name, std::string value) {
    tinyxml2::XMLElement *elem = getElementByPropertyFromNode(node, property_name);
    if (!elem) {
        // if elem does not exist, so cannot be set
        return -1;
    }
    elem->SetAttribute("value", value.c_str());
    if (!std::string(elem->FirstAttribute()->Next()->Value()).compare(value)) {
        return 0;  // return success
    } else {
        // value after set is incorrect or missing
        return -1;
    }
    return -2;  // unknown error; useful for debugging
}

/**
 *THIS IS FOR DEBUGGING
 *PURPOSES ONLY
 *PRINTS DIRECTLY TO STDOUT
 */

// print all attributes of an element.
// returns the number of attributes
// int print_attr(tinyxml2::XMLElement*element, unsigned int indent) {
//     if (element == NULL) return 0;

//     const tinyxml2::XMLAttribute*attribute = element->FirstAttribute();
//     int i = 0, intval;
//     double dbval;
//     // const char*pIndent = tinyxml2::getIndent(indent);
//     printf("\n");
//     while (attribute) {
//         printf("%u%s: value=[%s]", indent, attribute->Name(), attribute->Value());

//         if (attribute->QueryIntValue(&intval) == tinyxml2::XML_SUCCESS) printf( " int=%d", intval);
//         if (attribute->QueryDoubleValue(&dbval) == tinyxml2::XML_SUCCESS) printf( " d=%1.1f", dbval);
//         printf("\n");
//         i++;
//         indent++;
//         attribute = attribute->Next();
//     }
//     return i;
// }

// print the names of all children of an element
// returns the number of elements
// int print_elem(tinyxml2::XMLElement*element, unsigned int indent) {
//     if (element == NULL) return 0;

//     tinyxml2::XMLElement*subElement = element->FirstChildElement();
//     int i = 0, intval;
//     double dbval;
//     // const char*pIndent = tinyxml2::getIndent(indent);
//     printf("\n");
//     while (subElement) {
//         printf("%u - %s:", indent, subElement->Name());

//         // if (attribute->QueryIntValue(&intval) == tinyxml2::XML_SUCCESS) printf( " int=%d", intval);
//         // if (attribute->QueryDoubleValue(&dbval) == tinyxml2::XML_SUCCESS) printf( " d=%1.1f", dbval);
//         printf("\n");
//         i++;
//         indent++;
//         subElement = subElement->NextSiblingElement();
//     }
//     return i;
// }
