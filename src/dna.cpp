// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <stdio.h>

#include <cstring>
#include <string>
#include <vector>
#include <iostream>  // errors
#include <algorithm>

#include "../include/custom.hpp"
#include "../include/dna.hpp"

Dna::Dna() : dnas() {
    // dnas();
    // std::string str;
    // dnas[0].name = "9726 E473";
    // dnas[1].name = "57E6 300F";
    // dnas[0].ip = "192.168.0.11";
    // dnas[1].ip = "192.168.0.12";
    // dnas[0].mac = "02:00:00:00:00:11";
    // dnas[1].mac = "02:00:00:00:00:12";

    // unsigned i = 0;
    // while (i < dnas.size()) {
    //     dnas[i].db = i % 2;
    //     str = dnas[i].name;
    //     str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    //     dnas[i].file = str;
    //     std::cerr << dnas[i].file << std::endl;
    //     str = "";
    //     for (unsigned j = 0; j < 2; ++j) {
    //         dnas[i].pmt[j].gain = 128;  // default gain is 128
    //         dnas[i].pmt[j].pos = 2*i + j % 2;  // 0(0,1); 1(2,3); 2(4,5)...
    //     }
    //     i++;
    // }
}

Dna::~Dna() {}

void Dna::init() {
    // dnas = std::vector<dna_t> v();
    // dnas.clear();
    while (dnas.size() > 0) {  // more safe
        dnas.pop_back();
    }
    return;
}

dna_t Dna::getDNA(int which) {
    dna_t dummy = {};
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return dummy;
    }
    return dnas[which];
}

dna_t Dna::getDNA(std::string name) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].name == name || dnas[i].file == name) {
            return dnas[i];
        }
    }
    dna_t dummy = {};
    std::cerr << "ERROR: no DNA by the name: " << name << std::endl;
    return dummy;
}

std::vector<dna_t> Dna::getDNAs() {
    return dnas;
}

unsigned Dna::dnaNum() {
    return dnas.size();
}

int Dna::addDNA(std::string name) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(temp);
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    return 0;
}

int Dna::addDNA(std::string name, std::string ip, std::string mac) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(temp);
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name, ip and mac" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP(ip, dnas.size()-1);
    setMAC(mac, dnas.size()-1);
    setTriggerType(1, dnas.size()-1);
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    return 0;
}

int Dna::addDNA(std::string name, std::string ip, std::string mac, int on) {
    unsigned check = dnas.size();
    if (dnas.size() == 8) {
        std::cerr << "ERROR: Cannot add more DNAs" << std::endl;
        return -1;
    }
    dna_t temp = {};
    dnas.push_back(temp);
    if (check == dnas.size()) {
        std::cerr << "ERROR: Error adding DNA by name, ip, mac and on flag" << std::endl;
        return -2;
    }
    setName(name, dnas.size()-1);
    setDB(dnas.size()-1);
    setGain(dnas.size()-1, 0, 128);
    setGain(dnas.size()-1, 1, 128);
    setIP(ip, dnas.size()-1);
    setMAC(mac, dnas.size()-1);
    setTriggerType(((on == 1) ? 1 : 2), dnas.size()-1);  // set trig on/off state
    setTestPulse(false, dnas.size()-1);
    setDACval(100, dnas.size()-1);
    setHoldDelay(9, dnas.size()-1);
    setON(on, dnas.size()-1);
    return 0;
}

int Dna::addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs) {
    if (names.size() != ips.size() || macs.size() != ips.size()) {
        std::cerr << "ERROR: Error adding DNAs, details' sizes do not agree" << std::endl;
        return -1;
    }
    unsigned check = dnas.size();
    if (dnas.size() + names.size() > 8) {
        std::cerr << "ERROR: Cannot add so many DNAs" << std::endl;
        return -2;
    }
    for (unsigned i = 0; i < names.size(); ++i) {
        addDNA(names[i], ips[i], macs[i]);
    }
    if ((check + names.size()) != dnas.size()) {
        std::cerr << "ERROR: Adding DNAs failed" << std::endl;
        return -3;
    }
    return 0;
}

int Dna::addDNAs(std::vector<std::string> names, std::vector<std::string> ips, std::vector<std::string> macs,
    std::vector<int> ons) {
    if (names.size() != ips.size() || macs.size() != ips.size() || macs.size() != ons.size()) {
        std::cerr << "ERROR: Error adding DNAs, details' sizes do not agree" << std::endl;
        return -1;
    }
    unsigned check = dnas.size();
    if (dnas.size() + names.size() > 8) {
        std::cerr << "ERROR: Cannot add so many DNAs" << std::endl;
        return -2;
    }
    for (unsigned i = 0; i < names.size(); ++i) {
        addDNA(names[i], ips[i], macs[i], ons[i]);
    }
    if ((check + names.size()) != dnas.size()) {
        std::cerr << "ERROR: Adding DNAs failed" << std::endl;
        return -3;
    }
    return 0;
}

int Dna::setName(std::string name, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].name = name;
    setFileName(which);
    setFolderName(which);
    return 0;
}

int Dna::setName(std::string name, std::string which) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].name == which || dnas[i].file == which) {
            return setName(name, i);
        }
    }
    return -1;
}


std::string Dna::getName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].name;
}

std::vector<std::string> Dna::getNames() {
    std::vector<std::string> names;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        names.push_back(dnas[i].name);
    }
    return names;
}

int Dna::setCustomFileName(std::string file, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = file;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].file = str;
    return 0;
}

int Dna::setFileNames() {
    std::string str;
    unsigned i = 0;
    while (i < dnas.size()) {
        // dnas[i].db = i % 2;
        str = dnas[i].name;
        str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
        dnas[i].file = str;
        // std::cerr << dnas[i].file << std::endl;
        str = "";
        // for (unsigned j = 0; j < 2; ++j) {
        //     dnas[i].pmt[j].gain = 128;  // default gain is 128
        //     dnas[i].pmt[j].pos = 2*i + j % 2;  // 0(0,1); 1(2,3); 2(4,5)...
        // }
        i++;
    }
    return 0;
}

int Dna::setFileName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = dnas[which].name;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].file = str;
    return 0;
}

std::string Dna::getFileName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }

    return dnas[which].file;
}

std::vector<std::string> Dna::getFileNames() {
    std::vector<std::string> files;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        files.push_back(dnas[i].file);
    }
    return files;
}

int Dna::setCustomFolderName(std::string folder, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = folder;
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
    dnas[which].folder = str;
    return 0;
}

int Dna::setCustomFolderNames(std::string folder) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        setCustomFolderName(folder, i);
    }
    return 0;
}

int Dna::setFolderNames() {  // init or clear folder names
    std::string str;
    str = "";
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].folder = str;
    }
    return 0;
}

int Dna::setFolderName(int which) {  // auto method to init folder names
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    std::string str;
    str = "";
    dnas[which].folder = str;
    return 0;
}

std::string Dna::getFolderName(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return "";
    }
    return dnas[which].folder;
}

std::vector<std::string> Dna::getFolderNames() {
    std::vector<std::string> folders;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        folders.push_back(dnas[i].folder);
    }
    return folders;
}

int Dna::setDB(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].db = which % 2;
    return 0;
}

int Dna::getDB(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].db;
}

std::vector<int> Dna::getDBs() {
    std::vector<int> dbs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dbs.push_back(dnas[i].db);
    }
    return dbs;
}

int Dna::getON(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].on;
}

std::vector<int> Dna::getONs() {
    std::vector<int> ons;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        ons.push_back(dnas[i].on);
    }
    return ons;
}

std::vector<bool> Dna::getBoolONs() {
    std::vector<bool> ons;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].on == 0) {
            ons.push_back(false);
        } else {
            ons.push_back(true);
        }
    }
    return ons;
}

bool Dna::checkBoolONs() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].on == 0) return false;
    }
    return true;
}


int Dna::setGain(int which, int pmt, int gain) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    if (gain < 0 || gain > 255) {
        std::cerr << "ERROR: Invalid Gain value" << gain << std::endl;
        return -2;
    }
    if (pmt != 0 && pmt != 1) {
        std::cerr << "ERROR: Invalid PMT value" << pmt << std::endl;
        return -3;
    }
    dnas[which].pmt[pmt].gain = gain;
    dnas[which].pmt[pmt].pos = pmt;
    return 0;
}

int Dna::setGain(std::string name, int pmt, int gain) {
    int which = -1;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].name == name || dnas[i].file == name) {
            which = i;
            break;
        }
    }
    if (which == -1) {
        std::cerr << "ERROR: no DNA by the name: " << name << std::endl;
        return -1;
    }
    if (setGain(which, pmt, gain) < 0) {
        return -2;
    }
    return 0;
}

int Dna::setGainAll(int gain) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        for (unsigned j = 0; j < 2; ++j) {
            if (setGain(i, j, gain) < 0) return -1;
        }
    }
    return 0;
}

int Dna::getGain(int which, int pmt) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    if (pmt != 0 && pmt != 1) {
        std::cerr << "ERROR: Invalid PMT value" << pmt << std::endl;
        return -3;
    }
    return dnas[which].pmt[pmt].gain;
}

std::vector<int> Dna::getGainAll() {
    std::vector<int> gains;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        for (unsigned j = 0; j < 2; ++j) {
            gains.push_back(getGain(i, j));
        }
    }
    return gains;
}

int Dna::setIP(std::string ip, int which) {
    if (!checkIPAddress(ip)) {
        std::cerr << "ERROR: invalid IPs cannot be added in DNA" << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].ip = ip;
    return 0;
}

int Dna::setIP(std::string ip, std::string name) {
    if (!checkIPAddress(ip)) {
        std::cerr << "ERROR: invalid IPs cannot be added in DNA" << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].name == name || dnas[i].file == name) {
            dnas[i].ip = ip;
            return 0;
        }
    }
    std::cerr << "ERROR: IP not updated, no DNA by that name..." << std:: endl;
    return -1;
}
std::string Dna::getIP(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].ip;
}
std::vector<std::string> Dna::getIPs() {
    std::vector<std::string> ips;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        ips.push_back(dnas[i].ip);
    }
    return ips;
}

int Dna::setMAC(std::string mac, int which) {
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: invalid MACs cannot be added in DNA" << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].mac = mac;
    return 0;
}

int Dna::setMAC(std::string mac, std::string name) {
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: invalid MACs cannot be added in DNA" << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].name == name || dnas[i].file == name) {
            dnas[i].mac = mac;
            return 0;
        }
    }
    std::cerr << "ERROR: MAC not updated, no DNA by that name..." << std:: endl;
    return -1;
}
std::string Dna::getMAC(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return std::string("");
    }
    return dnas[which].mac;
}

std::vector<std::string> Dna::getMACs() {
    std::vector<std::string> macs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        macs.push_back(dnas[i].mac);
    }
    return macs;
}

int Dna::setTriggerType(int trig) {  // trig 1 is LVDS, 0 is Pulser, 2 is off
    if (trig < 0 && trig > 2) {
        std::cerr << "ERROR: Not a valid trigger type" << trig << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].trig = trig;
    }
    return 0;
}

int Dna::setTriggerType(int trig, int which) {  // trig 1 is LVDS, 0 is Pulser, 2 is off
    if (trig < 0 || trig > 2) {
        std::cerr << "ERROR: Not a valid trigger type" << trig << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].trig = trig;
    return 0;
}

int Dna::getTriggerType(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    return dnas[which].trig;
}

std::vector<int> Dna::getTriggerTypes() {
    std::vector<int> trigs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        trigs.push_back(dnas[i].trig);
    }
    return trigs;
}

std::vector<bool> Dna::getBoolTriggerTypes() {
    std::vector<bool> trigs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].trig == 1) {
            trigs.push_back(true);
        } else {
            trigs.push_back(false);
        }
    }
    return trigs;
}

int Dna::setTestPulse(bool on) {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].testpulse = on;
    }
    return 0;
}

int Dna::setTestPulse(bool on, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].testpulse = on;
    return 0;
}

bool Dna::checkTestPulse() {
    for (unsigned i = 0; i < dnas.size(); ++i) {
        if (dnas[i].testpulse == false) return false;
    }
    return true;
}

int Dna::checkTestPulse(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    return dnas[which].testpulse;
}

std::vector<bool> Dna::getTestPulse() {
    std::vector<bool> test;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        test.push_back(dnas[i].testpulse);
    }
    return test;
}

int Dna::setDACval(int DACval) {
    if (DACval < 0 || DACval > 255) {
        std::cerr << "ERROR: Invalid DAC value " << DACval << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].dac = DACval;
    }
    return 0;
}

int Dna::setDACval(int DACval, int which) {
    if (DACval < 0 || DACval > 255) {
        std::cerr << "ERROR: Invalid DAC value " << DACval << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].dac = DACval;
    return 0;
}

int Dna::getDACval(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }

    return dnas[which].dac;
}

std::vector<int> Dna::getDACvals() {
    std::vector<int> dacs;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dacs.push_back(dnas[i].dac);
    }
    return dacs;
}

int Dna::setHoldDelay(int hold_delay) {
    if (hold_delay < 0) {
        std::cerr << "ERROR: Invalid Hold Delay value " << hold_delay << std::endl;
        return -1;
    }
    for (unsigned i = 0; i < dnas.size(); ++i) {
        dnas[i].hold = hold_delay;
    }
    return 0;
}

int Dna::setHoldDelay(int hold_delay, int which) {
    if (hold_delay < 0) {
        std::cerr << "ERROR: Invalid Hold Delay value " << hold_delay << std::endl;
        return -1;
    }
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }
    dnas[which].hold = hold_delay;
    return 0;
}
int Dna::getHoldDelay(int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -2;
    }

    return dnas[which].hold;
}

std::vector<int> Dna::getHoldDelays() {
    std::vector<int> holds;
    for (unsigned i = 0; i < dnas.size(); ++i) {
        holds.push_back(dnas[i].hold);
    }
    return holds;
}

int Dna::setON(int on, int which) {
    if (which >= static_cast<int>(dnas.size()) || which < 0) {
        std::cerr << "ERROR: No DNA with index " << which << std::endl;
        return -1;
    }
    dnas[which].on = on;
    return 0;
}
