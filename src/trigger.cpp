/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
Konstantin Gizdov  2016 (Uni-Edinburgh)
*/

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>  // errors

#include "../include/trigger.hpp"
#include "../include/adept_depp.hpp"

AdeptDepp TriggerBoard::usb;

TriggerBoard::TriggerBoard() : switches(), count(0), counts(), gate_ca(), trg_none(false), trg_pulser(true), trg_beam(false),
trg_aux(false), led_en(false), boards() {
    for (unsigned i = 0; i < 8; ++i) {
        counts[i] = 0;
    }
    for (unsigned i = 0; i < 8; ++i) {
        gate_ca[i] = false;
    }
    // boards = {true, false, false, false, false, false, false, false};
    // boards[0] = true;
    setOnOffs(false);
    deadtime_ = 500;  // current default
    pulse_delay_ = 3500;
    usb = AdeptDepp();
    if ( !usb.OK() ) usb.close();
    if ( !usb.OK() ) usb.open();
    // initTrigger();
}

TriggerBoard::TriggerBoard(std::string deviceName) : switches(), count(0), counts(), gate_ca(), trg_none(false), trg_pulser(true), trg_beam(false),
trg_aux(false), led_en(false), boards() {
    for (unsigned i = 0; i < 8; ++i) {
        counts[i] = 0;
    }
    for (unsigned i = 0; i < 8; ++i) {
        gate_ca[i] = false;
    }
    // boards = {true, false, false, false, false, false, false, false};
    // boards[0] = true;
    setOnOffs(false);
    deadtime_ = 500;  // current default
    pulse_delay_ = 3500;
    usb = AdeptDepp(deviceName);
    if ( !usb.OK() ) usb.close();
    if ( !usb.OK() ) usb.open();
    // initTrigger();
}

TriggerBoard::~TriggerBoard() {
    usb.disconnect();
}

void TriggerBoard::initTrigger() {
    // if (prefs.getBoolean("trg_none",true)) setSwitchBits(0x0<<4,0x3<<4);
    // if (prefs.getBoolean("trg_pulser",true)) setSwitchBits(0x1<<4,0x3<<4);
    // if (prefs.getBoolean("trg_beam",true)) setSwitchBits(0x2<<4,0x3<<4);
    // if (prefs.getBoolean("trg_aux",true)) setSwitchBits(0x3<<4,0x3<<4);
    // if (prefs.getBoolean("led_en",true)) setSwitchBits(0x40,0x40);

    // for (int db=0;db<8;db++)
    // {
    //   int dbMask = 1<<(db+8);
    //   if (prefs.getBoolean("db"+db,false))
    //     { setSwitchBits(0x0,dbMask); }
    //   else
    //     { setSwitchBits(dbMask,dbMask); }
    // }

    // setDeadtime(prefs.getInt("deadtime",255));
    // setPulseDelay(prefs.getInt("pulse_delay",100));

    if (trg_none) setSwitchBits(0x0<<4, 0x3<<4);
    if (trg_pulser) setSwitchBits(0x1<<4, 0x3<<4);
    if (trg_beam) setSwitchBits(0x2<<4, 0x3<<4);
    if (trg_aux) setSwitchBits(0x3<<4, 0x3<<4);
    if (led_en) setSwitchBits(0x40, 0x40);

    for (int db=0; db < 8; db++) {
        int dbMask = 1 << (db+8);
        if (boards[db]) {  // each board can trigger 8 others, which ones do you choose
            setSwitchBits(0x0, dbMask);
        } else {
            setSwitchBits(dbMask, dbMask);
        }
    }

    setDeadtime(deadtime_);  // default 255
    setPulseDelay(pulse_delay_);  // default 100

    return;
}

bool TriggerBoard::reset() {
    usb.close();
    usb.open();
    return usb.OK();
}


void TriggerBoard::setSwitchBits(int bits, int mask) {
    // switches &= ~mask; // Force bits off
    // switches |= bits; // Update the bits
    // byte [] addr = {(byte)0x0,(byte)0x0,(byte)0x1,(byte)0x0};
    // byte [] buf = new byte [2];
    // buf[0] = (byte)(switches & 0xff);
    // buf[1] = (byte)((switches>>8) & 0xff);
    // try
    // {
    //   usb.put(addr);
    //   usb.push((byte)0x3,buf);
    // }
    // catch ( JniAdeptDepp.DeppException de )
    // { System.out.println("setSwitchBits() exception: bits="+bits+" mask="+mask); }
    switches &= ~mask;  // Force bits off
    switches |= bits;  // Update the bits
    // byte addr[4] = {(byte)0x0, (byte)0x0, (byte)0x1, (byte)0x0};
    byte buf[2];
    buf[0] = (byte)(switches & 0xff);
    buf[1] = (byte)((switches>>8) & 0xff);

    usb.putBytes(0x0, 0x0000, buf, 2);
    // usb.put(addr, 4);
    // usb.push((byte)0x3, buf, 2);

    return;
}

void TriggerBoard::setDeadtime(int deadtime) {
    // if (deadtime<1) deadtime = 1;
    // if (deadtime>0x1ff) deadtime = 0x1ff;
    // prefs.putInt("deadtime",deadtime);
    // byte [] addr = {(byte)0x0,(byte)0x6,(byte)0x1,(byte)0x0};
    // byte [] buf = new byte [2];
    // buf[0] = (byte)(deadtime & 0xff);
    // buf[1] = (byte)((deadtime>>8) & 0xff);
    // try
    // {
    //   usb.put(addr);
    //   usb.push((byte)0x3,buf);
    // }
    // catch ( JniAdeptDepp.DeppException de )
    // { System.out.println("deadtime exception: deadtime="+deadtime); }

    deadtime_ = deadtime;

    if (deadtime < 1) deadtime = 1;
    if (deadtime > 0x1ff) deadtime = 0x1ff;

    // byte addr[4] = {(byte)0x0, (byte)0x6, (byte)0x1, (byte)0x0};
    byte buf[2];
    buf[0] = (byte)(deadtime & 0xff);
    buf[1] = (byte)((deadtime>>8) & 0xff);

    usb.putBytes(0x0, 0x0006, buf, 2);
    usb.putByte(0x0, 0x0002, 0x80);
    // usb.put(addr, 4);
    // usb.push((byte)0x3, buf, 2);

    return;
}

void TriggerBoard::changeDeadtime(int deadtime) {
    deadtime_ = deadtime;
    return;
}

void TriggerBoard::setPulseDelay(int pulseDelay) {
    // if (pulseDelay<10) pulseDelay = 10;
    // if (pulseDelay>0xffff) pulseDelay = 0xffff;
    // prefs.putInt("pulse_delay",pulseDelay);
    // byte [] addr = {(byte)0x0,(byte)0x8,(byte)0x1,(byte)0x0};
    // byte [] buf = new byte [2];
    // buf[0] = (byte)(pulseDelay & 0xff);
    // buf[1] = (byte)((pulseDelay>>8) & 0xff);
    // try
    // {
    //   usb.put(addr);
    //   usb.push((byte)0x3,buf);
    // }
    // catch ( JniAdeptDepp.DeppException de )
    // { System.out.println("pulseDelay exception: delay="+pulseDelay); }

    pulse_delay_ = pulseDelay;

    if (pulseDelay < 10) pulseDelay = 10;
    if (pulseDelay > 0xffff) pulseDelay = 0xffff;

    // byte addr[4] = {(byte)0x0, (byte)0x8, (byte)0x1, (byte)0x0};
    byte buf[2];
    buf[0] = (byte)(pulseDelay & 0xff);
    buf[1] = (byte)((pulseDelay>>8) & 0xff);

    usb.putBytes(0x0, 0x0008, buf, 2);
    // usb.put(addr, 4);
    // usb.push((byte)0x3, buf, 2);

    return;
}

void TriggerBoard::changePulseDelay(int pulseDelay) {
    pulse_delay_ = pulseDelay;
    return;
}

void TriggerBoard::resetCounts() {
    toggleButtonBits(0x1);
    count = 0;
    for (unsigned i = 0; i < 8; ++i) {
        counts[i] = 0;
    }
}

void TriggerBoard::toggleButtonBits(int bits) {
    // try
    // {
    //   byte [] addr = {(byte)0x0,(byte)0x2,(byte)0x1,(byte)0x0};
    //   byte [] buf = new byte [2];
    //   buf[0] = (byte)(bits & 0xff);
    //   buf[1] = (byte)((bits>>8) & 0xff);
    //   usb.put(addr);
    //   usb.push((byte)0x3,buf);
    //   buf[0] = 0; buf[1] = 0; // Unset the bits
    //   usb.put(addr);
    //   usb.push((byte)0x3,buf);
    // }
    // catch ( JniAdeptDepp.DeppException de )
    // { System.out.println("toggleButtonBits() exception"); }
    // byte addr[4] = {(byte)0x0, (byte)0x2, (byte)0x1, (byte)0x0};
    byte buf[2];
    buf[0] = (byte)(bits & 0xff);
    buf[1] = (byte)((bits>>8) & 0xff);
    usb.putBytes(0x0, 0x0002, buf, 2);
    // usb.put(addr, 4);
    // usb.push((byte)0x3, buf, 2);
    // buf[0] = 0; buf[1] = 0;  // Unset the bits
    // usb.put(addr, 4);
    // usb.push((byte)0x3, buf, 2);
}

void TriggerBoard::updateCount() {
    // try {
    for (unsigned id = 0; id < 8; ++id) {
        byteint u;
        byte buf[4];
        byte addr[4] = {(byte)0x0, (byte)(id*4), (byte)0x1, (byte)0x0};
        usb.put(addr, 4);
        usb.pop((byte)0x2, buf, 4);
        // To combine the 4 signed bytes into an int, convert via a hex string
        // String countString = String.format("%1$02x%2$02x%3$02x%4$02x",buf[3],buf[2],buf[1],buf[0]);
        if ((buf[3] & 0x80) == 0) {
            for (unsigned i = 0; i < 4; ++i) {
                u.b[i] = buf[i];
            }
            counts[id] = u.i;
        }
    }
    count = * std::max_element(counts, counts+8);
    // } catch ( JniAdeptDepp.DeppException de ) {
      //    System.out.println("updateCount() exception");
    // }
    return;
}

int TriggerBoard::getCount() {
    return count;
}

std::vector<int> TriggerBoard::getCounts() {
    std::vector<int> v;
    for (unsigned i = 0; i < 8; ++i) {
        v.push_back(counts[i]);
    }
    return v;
}

int TriggerBoard::getCount(int id) {
    if (id < 0 || id > 7) {
        std::cerr << "ERROR: No counter with such id exists..." << std::endl;
        return -1;
    }
    return counts[id];
}

void TriggerBoard::updateGateCA() {
    byte buf[1];
    byte addr[4] = {(byte)0x0, (byte)(46), (byte)0x1, (byte)0x0};

    usb.put(addr, 4);
    usb.pop((byte)0x2, buf, 1);

    for (int g = 0; g < 8; g++) {
        if ( (buf[0] & (1 << g)) == 0 ) {
            gate_ca[g] = true;
        } else {
            gate_ca[g] = false;
        }
    }

    return;
}

int TriggerBoard::setOnOff(bool on, int which) {
    if (which >= 8 || which < 0) {
        std::cerr << "ERROR: No board with index " << which << std::endl;
        return -1;
    }
    boards[which] = on;
    return 0;
}

void TriggerBoard::setOnOffs(std::vector<bool> ons) {
    for (unsigned i = 0; i < ons.size(); ++i) {
        boards[i] = ons[i];
    }
    return;
}

void TriggerBoard::setOnOffs(bool on) {
    for (unsigned i = 0; i < 8; ++i) {
        boards[i] = on;
    }
    return;
}

void TriggerBoard::changeTrig(bool test) {
    if (test) {
        trg_beam = true;
        trg_pulser = false;
    } else {
        trg_beam = false;
        trg_pulser = true;
    }
    return;
}

// void TriggerBoard::updateGateCA() {
//     byte buf[1];
//     byte addr[] = {(byte)0x0, (byte)(46), (byte)0x1, (byte)0x0};
//     try {
//         usb.put(addr, 4);
//         usb.pop((byte)0x2, buf);
//         for (unsigned g = 0; g < 8; g++) {
//             if ( (buf[0] & (1 << g)) == 0 ) {
//                 gateCA.setCellOff(g, 0);
//             } else {
//                 gateCA.setCellOn(g, 0);
//             }
//         }
//     } catch ( JniAdeptDepp.DeppException de ) {
//       //    System.out.println("updateCount() exception");
//     }
//     return;
// }

