// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "../include/rich_control.hpp"
#include "../include/custom.hpp"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"
#include "../include/trigger.hpp"
#include "../include/dna.hpp"

int main() {
    int pipeio[2], pipeio1[2], fifo;  // 0 - read, 1 - write
    // over is_over;
    // is_over.info.ask = 0;  // 0 ask whether to quit
    // is_over.info.tell = 1;  // 1 - continue, 0 - quit
    pid_t pid;
    pthread_t thread_usb, thread_fifo;
    pthread_attr_t attrs_usb, attrs_fifo;
    pthread_attr_init(&attrs_usb);
    pthread_attr_init(&attrs_fifo);
    void *status_usb, *status_fifo;

    pipe(pipeio);
    pipe(pipeio1);

    // fork for proxy
    pid = fork();

    if (pid < 0) {
        std::cerr << "FORK FAILED" << std::endl;
        return -1;
    }

    if (pid == 0) {  // Child
        // execl("startProxy.sh","--protocol","0xf4", (char*) NULL);
        dup2(pipeio[0], STDIN_FILENO);
        close(pipeio[1]);
        dup2(pipeio1[1], STDOUT_FILENO);
        close(pipeio1[0]);
        execl("startProxy.sh", "--protocol", "0xf4", static_cast<char*>(NULL));
        std::cerr << "THIS SHOULD NOT PRINT\n" << std::endl;
        return -1;
    }
    inchannel = dup(pipeio1[0]);
    close(pipeio1[1]);
    outchannel = dup(pipeio[1]);
    close(pipeio[0]);

    // Dna dnaStore;
    dnaStore.init();  // initiate DNAs();

    // TriggerBoard trigB = TriggerBoard();
    pthread_mutex_lock(&wait_usb);
    TriggerBoard trigB;
    trigB.initTrigger();
    pthread_mutex_unlock(&wait_usb);

    tinyxml2::XMLError errCheck = settings.LoadFile("./settings/settings_template.xml");  // load settings file
    if (errCheck != 0) {
        errCheck = settings.LoadFile("../settings/settings_template.xml");  // try in a different folder if needed
    }
    XMLCheckSuccess(errCheck);  // check if file is loaded

    errCheck = marocPD.LoadFile("./settings/RICH-MAROC3-PD-NEW.xml");  // load channel mapping file
    if (errCheck != 0) {
        errCheck = marocPD.LoadFile("../settings/RICH-MAROC3-PD-NEW.xml");  // try in a different folder if needed
    }
    XMLCheckSuccess(errCheck);  // check if file is loaded

    std::string docName = "./settings/commonPrefs.xml";

    if (readCommonPrefs(&prefsdoc, docName, &commonPrefs) != 0) {  // load runtime variables
        printf("ERROR: Could not load common preferences...Exiting.\n");
        return -1;
    }

    runNumber = commonPrefs.runtime.runNumber;

    std::string fifo_name = commonPrefs.proxy.trig.fifo;
    // make sure name is not occupied
    unlink(fifo_name.c_str());
    // Create and open read/write so that the open does not block while waiting for a reader
    fifo = open(fifo_name.c_str(), O_RDWR|O_TRUNC|O_NONBLOCK|O_CREAT, 0666);
    if (fifo < 0) {
        std::cerr << strerror(errno) << std::endl;
        std::cerr << "ERROR: (" << fifo << ") Could not open FIFO. Exiting..." << std::endl;
        std::cerr << "Killing children and closing resources..." << std::endl;
        kill(pid, SIGTERM);  // terminate proxy
        // close and unlink FIFO
        close(fifo);
        unlink(fifo_name.c_str());
        std::cerr << "Bye!" << std::endl;
        exit(-1);
    }
    pthread_create(&thread_fifo, &attrs_fifo, &trigCountFIFO, &fifo);

    int configured = 0;
    bool debug = true;
    char * commBuf = new char[1024];
    msg_u msg;
    bool quit = false;
    std::string folder, name, ip, mac;
    std::vector<std::string> names, ips, macs;
    std::vector<int> is_ons;

    bool comm_ok = true;  // track quality of commands
    while (!quit) {
        for (unsigned received = 0; received < sizeof(msg_t); ) {
            if (!comm_ok) break;  // quit on bad command
            size_t len = sizeof(msg_t) - received;
            len = read(0, msg.b+received, len);

            if (len == 0) {
                std::cerr << "ERROR: Encountered EOL, could not read command. Exiting..." << std::endl;
                // std::cout << "ct" << std::endl;  // error command type
                comm_ok = false;
                break;
            }
            if (len > 0) received += len;
        }

        uint16_t length = 0, comm = 17;  // assume failed read, comm = 17 to quit gracefully
        uint32_t info, seq;
        if (comm_ok) {
            length = msg.inf.length;  // length of following message
            comm = msg.inf.comm;      // current command type
            info = msg.inf.info;      // accompanying info
            seq = msg.inf.seq;        // exchange sequence number
        }
        // } else {
        //     comm = 17;  // quit gracefully if command fails
        // }

        if (length > 0) {  // only true if comm_ok
            for (unsigned received = 0; received < length;) {
                size_t len = length - received;
                // len = read(0, commBuf + 2 + received, len);
                len = read(0, commBuf + received, len);
                if (len == 0) {
                    std::cerr << "ERROR: Could not read full message length. Try again..." << std::endl;
                    // std::cout << "ml" << std::endl;  // error message length
                    comm_ok = false;
                    comm = 17;
                    break;
                }
                if (len > 0) received += len;
            }
        }
        // if (!comm_ok) comm = 17;  // quit on bad command

        if (debug) {
            std::cerr << "DEBUG: command type [length, command, info, sequence] -> "
            << length << " " << comm << " " << info << " " << seq << std::endl;
        }

        switch (comm) {
            case 1: {  // configure boards (long)
                if (debug) {
                    std::cerr << "DEBUG: command buffer -> " << commBuf << std::endl;
                }
                dnaStore.init();  // clear the previous boards
                folder.clear();
                names.clear();
                ips.clear();
                macs.clear();
                is_ons.clear();
                unsigned end_check = 0;
                unsigned on_check = 0;
                std::istringstream commands(commBuf);
                std::string buf;
                std::getline(commands, buf, ';');
                std::istringstream fold(buf);
                fold >> folder >> end_check;
                commonPrefs.runtime.saveFileDestination = folder;
                if (debug) {
                    std::cerr << "DEBUG: folder -> "<< folder << std::endl;
                }
                std::getline(commands, buf, ';');
                std::string curr_ip, curr_mac;
                std::istringstream ip_mac(buf);
                ip_mac >> curr_ip >> curr_mac >> end_check;
                if (debug) {
                    std::cerr << "DEBUG: PC IP, MAC -> " << curr_ip << ", " << curr_mac << std::endl;
                }
                map.setPC_IP(curr_ip);
                map.setPC_MAC(curr_mac);
                // commBuf = commBuf + folder.length() + 1 + 2;  // end_check should always be 1 char
                while (end_check == 0) {
                    name.clear();
                    ip.clear();
                    mac.clear();
                    std::getline(commands, name, ',');
                    std::getline(commands, buf, ';');
                    std::istringstream str(buf);
                    str >> ip >> mac >> on_check >> end_check;
                    // if (on_check == 0) {
                    //     continue;
                    // }
                    names.push_back(name);
                    ips.push_back(ip);
                    macs.push_back(mac);
                    is_ons.push_back(on_check);
                    if (debug) {
                        std::cerr << "DEBUG: DNA name, IP, MAC, ON/OFF -> "
                        << name << ", " << ip << ", " << mac << ", " << on_check << std::endl;
                    }
                    // commBuf = commBuf + name.length() + ip.length() + mac.length() + 1 + 1 + 5;  // on_check & end_check should always be 1 char
                }
                if (dnaStore.addDNAs(names, ips, macs, is_ons) != 0) {
                    std::cerr << "ERROR: DNAs not accepted. Try again..." << std::endl;
                    // std::cout << "dn" << std::endl;  // error dna not accepted
                    writeToParent(msg, "dn", 1);
                    break;
                }
                dnaStore.setCustomFolderNames(folder);
                configured++;  // keep track if config is received
                // std::cout << "ok" << std::endl;  // respond with ok
                writeToParent(msg, "ok", 0);
                break;
            }
            case 2: {  // configure params (short)
                if (debug) {
                    std::cerr << "DEBUG: command buffer -> " << commBuf << std::endl;
                }
                std::istringstream commands(commBuf);
                std::string buf;
                std::vector<int> gains(16);
                std::getline(commands, buf, ';');  // get gains
                std::istringstream buf_(buf);
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    std::getline(buf_, buf, ',');
                    std::istringstream gain(buf);
                    gain >> gains[2*i] >> gains[2*i+1];
                    if (debug) {
                        std::cerr << "DEBUG: gains -> " << gains[2*i] << " " << gains[2*i+1] << std::endl;
                    }
                }
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {  // take care of orientation
                    // if (i % 2 == 0) {
                        dnaStore.setGain(i, 0, gains[2*i]);
                        dnaStore.setGain(i, 1, gains[2*i+1]);
                    // } else {
                    //     dnaStore.setGain(i, 1, gains[2*i]);
                    //     dnaStore.setGain(i, 0, gains[2*i+1]);
                    // }
                }

                std::getline(commands, buf, ';');  // get test pulses
                std::istringstream tests_(buf);
                std::vector<bool> tests(8);
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    std::getline(tests_, buf, ',');
                    std::istringstream test(buf);
                    bool test_;
                    test >> test_;
                    tests[i] = test_;
                    if (debug) {
                        std::cerr << "DEBUG: test pulses -> " << tests[i] << std::endl;
                    }
                }
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    dnaStore.setTestPulse(tests[i], i);
                }

                std::getline(commands, buf, ';');  // get dacvals
                std::istringstream dacs_(buf);
                std::vector<int> dacs(8);
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    std::getline(dacs_, buf, ',');
                    std::istringstream dac(buf);
                    dac >> dacs[i];
                    if (debug) {
                        std::cerr << "DEBUG: DACs -> " << dacs[i] << std::endl;
                    }
                }
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    dnaStore.setDACval(dacs[i], i);
                }

                std::getline(commands, buf, ';');  // get holds
                std::istringstream holds_(buf);
                std::vector<int> holds(8);
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    std::getline(holds_, buf, ',');
                    std::istringstream hold(buf);
                    hold >> holds[i];
                    if (debug) {
                        std::cerr << "DEBUG: hold delays -> " << holds[i] << std::endl;
                    }
                }
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    dnaStore.setHoldDelay(holds[i], i);
                }

                std::getline(commands, buf, ';');  // get trig types
                std::vector<int> trigs(8);
                std::istringstream trigs_(buf);
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    std::getline(trigs_, buf, ',');
                    std::istringstream trig(buf);
                    trig >> trigs[i];
                    if (debug) {
                        std::cerr << "DEBUG: triggers -> " << trigs[i] << std::endl;
                    }
                }
                for (unsigned i = 0; i < dnaStore.dnaNum(); ++i) {
                    dnaStore.setTriggerType(trigs[i], i);
                }

                int pulse_count_threshold = 0, deadtime = 100, pulseDelay = 3500;
                std::getline(commands, buf, ';');  // get trig other params
                std::istringstream other_(buf);
                other_ >> pulse_count_threshold >> deadtime >> pulseDelay;
                if (debug) {
                    std::cerr << "DEBUG: pulse delay, pulse count threshold, deadtime -> "
                    << pulseDelay << ", " << pulse_count_threshold << ", " << deadtime << std::endl;
                }
                commonPrefs.runtime.pulseCount = pulse_count_threshold;  // make sure to update runtime settings
                commonPrefs.runtime.pulseDelay = pulseDelay;
                commonPrefs.runtime.pulseDelay = dnaStore.checkBoolONs();

                pthread_mutex_lock(&wait_threshold);
                threshold = pulse_count_threshold;
                pthread_mutex_unlock(&wait_threshold);
                pthread_mutex_lock(&wait_usb);
                trigB.changeDeadtime(deadtime);
                trigB.changePulseDelay(pulseDelay);
                trigB.setOnOffs(dnaStore.getBoolTriggerTypes());
                trigB.initTrigger();
                sleep(1);
                pthread_mutex_unlock(&wait_usb);
                configured++;  // keep track if config is received
                // std::cout << "ok" << std::endl;
                writeToParent(msg, "ok", 0);
                break;
            }
            case 3: {  // increment run number manually
                if (info) {
                    recording = true;
                    commonPrefs.runtime.recording = recording;
                } else {
                    recording = false;
                    commonPrefs.runtime.recording = recording;
                }
                pthread_mutex_lock(&wait_talk);
                commonPrefs.runtime.runNumber = NewRun();
                pthread_mutex_unlock(&wait_talk);
                // std::cout << "ok" << std::endl;
                writeToParent(msg, "ok", runNumber);
                break;
            }
            case 4: {  // start new run
                if (configured < 2) {
                    std::cerr << "ERROR: Not configured. Try again..." << std::endl;
                    // std::cout << "cf" << std::endl;
                    writeToParent(msg, "cf", 1);
                    break;
                }
                if (info) {
                    recording = true;
                    commonPrefs.runtime.recording = recording;
                } else {
                    recording = false;
                    commonPrefs.runtime.recording = recording;
                }
                pthread_mutex_lock(&wait_thread);
                bool is_stopped = !is_running;
                pthread_mutex_unlock(&wait_thread);
                if (is_stopped == false) {
                    std::cerr << "ERROR: Acquisition still running. Stopping it first..." << std::endl;
                    pthread_mutex_lock(&wait_talk);
                    EndRun(true, commonPrefs, &settings, &marocPD, dnaStore);
                    pthread_mutex_unlock(&wait_talk);
                    pthread_mutex_lock(&wait_stop);
                    stop = true;
                    pthread_mutex_unlock(&wait_stop);
                    sleep(1);
                    if (handle_join(getVarName(thread_usb), pthread_join(thread_usb, &status_usb)) != 0) {
                        std::cerr << "ERROR: Unable to stop thread. Try again..." << std::endl;
                        // std::cout << "tp" << std::endl;  // error thread stop
                        writeToParent(msg, "tp", 1);
                        break;
                    }
                }
                pthread_mutex_lock(&wait_thread);
                bool thread_running = is_running;
                pthread_mutex_unlock(&wait_thread);
                if (!thread_running) {
                    pthread_mutex_lock(&wait_stop);
                    stop = false;
                    pthread_mutex_unlock(&wait_stop);
                    int rc = pthread_create(&thread_usb, &attrs_usb, &checkCounter, &trigB);
                    if (rc) {
                        std::cerr << "ERROR: Unable to start thread. Try again..." << std::endl;
                        // std::cout << "ts" << std::endl;  // error thread start
                        writeToParent(msg, "ts", 1);
                        break;
                    }
                }
                pthread_mutex_lock(&wait_talk);
                SetupProxy(true, commonPrefs, dnaStore);
                pthread_mutex_unlock(&wait_talk);
                // Need to store settings
                storeRunSettings(&prefsdoc, &settings, &marocPD, dnaStore);
                // start run
                pthread_mutex_lock(&wait_talk);
                StartRun(true, commonPrefs, &settings, &marocPD, dnaStore);
                pthread_mutex_unlock(&wait_talk);
                commonPrefs.runtime.runNumber = runNumber;
                saveCommonPrefs(&prefsdoc, docName, commonPrefs);
                // std::cout << "ok" << std::endl;
                writeToParent(msg, "ok", runNumber);
                break;
            }
            case 5: {  // stop
                // int rc = 0;
                pthread_mutex_lock(&wait_thread);
                bool thread_running = is_running;
                pthread_mutex_unlock(&wait_thread);
                if (thread_running) {
                    pthread_mutex_lock(&wait_stop);
                    stop = true;
                    pthread_mutex_unlock(&wait_stop);
                    sleep(1);
                    if (handle_join(getVarName(thread_usb), pthread_join(thread_usb, &status_usb)) != 0) {
                        std::cerr << "ERROR: Unable to stop thread. Try again..." << std::endl;
                        // std::cout << "tp" << std::endl;  // error thread stop
                        writeToParent(msg, "tp", 1);
                        break;
                    }
                }
                pthread_mutex_lock(&wait_talk);
                SetupProxy(true, commonPrefs, dnaStore);
                pthread_mutex_unlock(&wait_talk);
                // end run
                pthread_mutex_lock(&wait_talk);
                EndRun(true, commonPrefs, &settings, &marocPD, dnaStore);
                pthread_mutex_unlock(&wait_talk);
                // std::cout << "ok" << std::endl;
                writeToParent(msg, "ok", 0);
                break;
            }
            case 17: {  // quit - keep 17 as the letter Q
                // ends run, joins threads and quits
                pthread_mutex_lock(&wait_talk);
                EndRun(true, commonPrefs, &settings, &marocPD, dnaStore);
                pthread_mutex_unlock(&wait_talk);
                int rc = 0, rc_fifo = 0;
                pthread_mutex_lock(&wait_thread);
                bool thread_running = is_running;
                pthread_mutex_unlock(&wait_thread);
                pthread_mutex_lock(&wait_parent);
                parent = false;
                pthread_mutex_unlock(&wait_parent);
                if (thread_running) {
                    pthread_mutex_lock(&wait_stop);
                    stop = true;
                    pthread_mutex_unlock(&wait_stop);
                    sleep(1);
                }
                rc      = handle_join(getVarName(thread_usb), pthread_join(thread_usb, &status_usb));
                rc_fifo = handle_join(getVarName(thread_fifo), pthread_join(thread_fifo, &status_fifo));
                if ((rc || rc_fifo) && comm_ok) {
                    // std::cout << "tp" << std::endl;
                    writeToParent(msg, "tp", 1);
                    break;
                }
                quit = true;
                // std::cout << "ok" << std::endl;
                writeToParent(msg, "ok", 0);
                break;
            }
            default: {
                std::cerr << "ERROR: Unknown message received. Try again..." << std::endl;
                std::cout << "um" << std::endl;  // error unknown message
                break;
            }
        }
    }

    // kill(pid, SIGQUIT);  // quit and dump proxy
    kill(pid, SIGTERM);  // terminate proxy

    iterateRun(&prefsdoc, docName);

    // close and unlink FIFO
    close(fifo);
    unlink(fifo_name.c_str());

    // std::cout << "x";  // signal exit

    delete[] commBuf;
    // EXIT SUCCESS
    return 0;
}
