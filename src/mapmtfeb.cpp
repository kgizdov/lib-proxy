/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>
#include <string>

#include "../include/custom.hpp"
#include "../include/mapmtfeb.hpp"

#include "../include/mapmtfeb.h"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"

// OR
//
// #include "mapmtfebcsr.h"

// using byte = uint8_t;  // define byte type

// struct mapmtfebControl_t
// {
//     febmap_t feb;
//     maroc_ctl_t mar_ctl;
//     bb_ctl_t bbctl;
// };

// mapmtfebControl_t mapmtControl;

// mapmtfebStatus_u_t mapmtfebStatus;
// l1ChannelStatus_t channelStatus[4][12];

void getIPFromString(std::string ip, int ipArray[]) {
    // IPv4 demo of inet_ntop() and inet_pton()

    struct sockaddr_in sa;

    // store this IP address in sa:
    if (!inet_pton(AF_INET, ip.c_str(), &(sa.sin_addr))) {
        std::cerr << "ERROR: Not a valid IP address... Aborting" << std::endl;
        return;
    }

    // read long int byte by byte into array
    ipArray[3] = sa.sin_addr.s_addr & 0xFF;
    ipArray[2] = (sa.sin_addr.s_addr >> 8) & 0xFF;
    ipArray[1] = (sa.sin_addr.s_addr >> 16) & 0xFF;
    ipArray[0] = (sa.sin_addr.s_addr >> 24) & 0xFF;
    // printf("%d.%d.%d.%d\n", ipArray[0], ipArray[1], ipArray[2], ipArray[3]);
    return;
}

void getMACFromString(std::string mac, int macArray[]) {
    // strip string and store in int array
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: Not a valid MAC address... Aborting" << std::endl;
        return;
    }
    for (int i = 0; i < 6; ++i) {
        macArray[5-i] = strtoul(mac.substr(3*i, 2).c_str(), 0, 16);
    }
    return;
}

MAPMTFEB::MAPMTFEB() : mapmtControl(), pc() {
    pc.mac = "E8:DE:27:01:AB:A3";
    pc.ip  = "192.168.0.3";
}

int MAPMTFEB::setPC_MAC(std::string mac) {
    if (!checkMACAddress(mac.c_str())) {
        std::cerr << "ERROR: Not a valid MAC address... Aborting" << std::endl;
        return -1;
    }

    pc.mac = mac;

    if (pc.mac != mac) {
        std::cerr << "ERROR: Could not set MAC address... Aborting" << std::endl;
        return -2;
    }
    return 0;
}

std::string MAPMTFEB::getPC_MAC() {
    return pc.mac;
}

int MAPMTFEB::setPC_IP(std::string ip) {
    // IPv4 check
    if (!checkIPAddress(ip)) {
        std::cerr << "ERROR: Not a valid IP address... Aborting" << std::endl;
        return -1;
    }

    pc.ip = ip;

    if (pc.ip != ip) {
        std::cerr << "ERROR: Could not set IP address... Aborting" << std::endl;
        return -2;
    }
    return 0;
}

std::string MAPMTFEB::getPC_IP() {
    return pc.ip;
}

MAPMTFEB::~MAPMTFEB() {}

void MAPMTFEB::changeTrigger(int trigtype) {  // 1 is LVDS, 0 is pulser
    bool trig = (trigtype == 1) ? 1 : 0;
    int sources_en = 0;
    int sources_cmpl = 0;
    int sources_trg = 0;
    bool temp = false;
    for (int i = 0; i < 8; i++) {
        // sources_en = sources_en | ((prefs.getBoolean("db.source.enable"+i,false) ? 1<<i : 0));
        // sources_cmpl = sources_cmpl | ((prefs.getBoolean("db.source.complement"+i,false) ? 1<<i : 0));
        // sources_trg = sources_trg | ((prefs.getBoolean("db.source.trigger"+i,false) ? 1<<i : 0));
        if (i == 3) {
            temp = trig;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
            temp = false;
        } else if (i == 6) {
            temp = !trig;
            if (trigtype == 2) temp = false;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
            temp = false;
        } else if (i == 7) {
            temp = true;
            if (trigtype == 2) temp = false;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((temp ? 1 << i : 0));
            temp = false;
        } else {
            sources_en = sources_en | ((false ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
        }
    }
    setSourcesEnable(sources_en);
    setSourcesComplement(sources_cmpl);
    setSourcesTrigger(sources_trg);
    return;
}

// void MAPMTFEB::setStateFromPreferences(Preferences prefs, Preferences prefsCommon, Preferences prefsRuntime) {  // OLD LINE FOR REFERENCE
void MAPMTFEB::setStateFromPreferences(const commonPrefsStruct& commonPrefs, dna_t dna) {
    int mac[6];
    int ip[4];

    std::string d_mac_str = pc.mac;  // destination is the PC
    std::string d_ip_str = pc.ip;  // destination

    std::string s_mac_str = dna.mac;  // source is the board
    std::string s_ip_str = dna.ip;  // source

    getMACFromString(d_mac_str, mac);
    setDestinationMacAddress(mac);

    getIPFromString(d_ip_str, ip);
    setDestinationIpAddress(ip);

    getMACFromString(s_mac_str, mac);
    setSourceMacAddress(mac);

    getIPFromString(s_ip_str, ip);
    setSourceIpAddress(ip);

    // setIpProtocol(prefsCommon.getInt("net.ip.protocol",0xf2));
    // setIpProtocol(0xf2);
    setIpProtocol(0xf2);  // static for now
    // setIpTtl(prefsCommon.getInt("net.ip.ttl",64));
    setIpTtl(64);  // static for now

    // setDummyIngressSize(Integer.parseInt(prefsCommon.get("runtime.dummy-ingress-size","10"),16));
    setDummyIngressSize(static_cast<int>(strtoul("10", 0, 16)));  // static for now

    // setTruncationHwm(Integer.parseInt(prefsCommon.get("mep.truncation-hwm","1000"),16));
    setTruncationHwm(static_cast<int>(strtoul("1000", 0, 16)));  // static for now

    // setMepEventCount(Integer.parseInt(prefsCommon.get("mep.events","1")));
    // setMepEventCount(commonPrefs.runtime.maxEvents);
    setMepEventCount(1);  // static for now

    // setPulseCount( prefsRuntime.getInt( "runtime.pulseCount" , 100 ) );
    setPulseCount(commonPrefs.runtime.pulseCount);
    // setPulseDelay( prefsRuntime.getInt( "runtime.pulseDelay" , 100 ) );
    setPulseDelay(commonPrefs.runtime.pulseDelay);
    // setLatency( prefsRuntime.getInt( "runtime.latency" , 16 ) );
    setLatency(commonPrefs.runtime.latency);
    // setTriggerDelay( prefsRuntime.getInt( "runtime.triggerDelay" , 16 ) );
    setTriggerDelay(commonPrefs.runtime.triggerDelay);
    // setStrobeLength(prefsRuntime.getInt("runtime.strobeLength",7));
    setStrobeLength(commonPrefs.runtime.strobeLength);

    // setInvertFE( prefs.getBoolean( "db.invert-fe", false ) );  // set if signal is 1 or 0
    setInvertFE(false);
    // setEnableMIIM( prefs.getBoolean( "db.enable-miim", false ) );
    setEnableMIIM(false);

    int sources_en = 0;
    int sources_cmpl = 0;
    int sources_trg = 0;
    bool temp = false;
    for (int i = 0; i < 8; i++) {
        // sources_en = sources_en | ((prefs.getBoolean("db.source.enable"+i,false) ? 1<<i : 0));
        // sources_cmpl = sources_cmpl | ((prefs.getBoolean("db.source.complement"+i,false) ? 1<<i : 0));
        // sources_trg = sources_trg | ((prefs.getBoolean("db.source.trigger"+i,false) ? 1<<i : 0));
        if (i == 3) {
            temp = true;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
            temp = false;
        } else if (i == 6) {
            temp = false;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
            temp = false;
        } else if (i == 7) {
            temp = true;
            sources_en = sources_en | ((temp ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((temp ? 1 << i : 0));
            temp = false;
        } else {
            sources_en = sources_en | ((false ? 1 << i : 0));
            sources_cmpl = sources_cmpl | ((false ? 1 << i : 0));
            sources_trg = sources_trg | ((false ? 1 << i : 0));
        }
    }
    setSourcesEnable(sources_en);
    setSourcesComplement(sources_cmpl);
    setSourcesTrigger(sources_trg);

    // setSkipSmb( prefs.getBoolean( "db.skip-smb", false ) );
    setSkipSmb(false);

    // setClaroBaseAddr( prefs.getInt("db.bb.claro-select.i2caddr",0) );
    setClaroBaseAddr(66);
    // setDACBaseAddr( prefs.getInt("db.bb.dac.i2caddr",0) );
    setDACBaseAddr(152);
    // setDACval( prefs.getInt("db.bb.dac.value",0) );
    setDACval(100);
    // setTestPulseEnable( prefs.getBoolean( "db.testpulse", false ) );
    setTestPulseEnable(false);

    // setMarCmuxrpt(prefs.getBoolean("db.maroc.cmuxrpt",false),1);
    setMarCmuxrpt(false, 1);
    // setMarCmuxrpt(prefs.getBoolean("db.maroc.cmuxrpt",false),2);
    setMarCmuxrpt(false, 2);
    // setMarCmuxStop(prefs.getInt("db.maroc.cmuxstop",127));
    setMarCmuxStop(127);
    // setMarSkipCfg(prefs.getBoolean("db.maroc.skipcfg",false),1);
    setMarSkipCfg(false, 1);
    // setMarSkipCfg(prefs.getBoolean("db.maroc.skipcfg",false),2);
    setMarSkipCfg(false, 2);
    // setMarAdcEnable(1,prefs.getBoolean("db.maroc.adc-enable.1",false));
    setMarAdcEnable(1, true);
    // setMarAdcEnable(2,prefs.getBoolean("db.maroc.adc-enable.2",false));
    setMarAdcEnable(2, true);

    setMarHoldDelay(11);  // default value 127 for now

    return;
}

void MAPMTFEB::getRegisterArray(int regs[]) {
    size_t len;
    // int *array;  // not needed

    // Copy entire register map to the Java array

    // len = env->GetArrayLength( regs );
    // array = env->GetIntArrayElements( regs, 0 );
    len = 72;

    for (unsigned i=0; i < len; i++) {
        // array[i] = reinterpret_cast<unsigned char *>(&mapmtControl)[i];
        regs[i] = reinterpret_cast<unsigned char *>(&mapmtControl)[i];  // read the bytes of mapmtControl one by one
    }

    // env->ReleaseIntArrayElements( regs, array, 0 );

    return;
}

void MAPMTFEB::getRegisterArray(byte regs[], int nreg, int off) {
    size_t len;
    // byte *array;  // not needed

    // Copy entire register map to the Java array

    len = 72;
    // array = env->GetByteArrayElements( regs, 0 );

    for (unsigned i = 0; (i+off < len) && (i < sizeof(mapmtfebControl_t)) && (static_cast<int>(i) < nreg); i++) {
        // array[i+off] = reinterpret_cast<unsigned char *>(&mapmtControl)[i];  // read the bytes of mapmtControl one by one
        regs[i+off] = reinterpret_cast<unsigned char *>(&mapmtControl)[i];  // read the bytes of mapmtControl one by one
    }

    // env->ReleaseByteArrayElements( regs, array, 0 );

    return;
}

void MAPMTFEB::setECVersion(int number) {
    mapmtControl.fe.claro.ec_version = (number <= 2) ? 0 : 1;
    return;
}

int MAPMTFEB::getECVersion() {
    return mapmtControl.fe.claro.ec_version;
}

void MAPMTFEB::setClaroBaseAddr(int number) {
    mapmtControl.bbctl.bb_cs_smb_address = number;
    return;
}

void MAPMTFEB::setDACBaseAddr(int number) {
    mapmtControl.bbctl.bb_dac_smb_addr = number;
    return;
}

void MAPMTFEB::setDACval(int number) {
    int newnumber = 0;
    if (number < 0) {
        newnumber = 0;
    } else if (number > 1023) {
        newnumber = 1023;
    } else {
        newnumber = number;
    }

    mapmtControl.bbctl.bb_dac_value = newnumber;
    return;
}

//  void MAPMTFEB::setAliceMode (, jboolean flag)
//{
//  mapmtControl.egress_ctl2.lhcb_alice = flag ? 1 : 0;
//  return;
//}

//  jboolean getAliceMode ()
//{
//  return (mapmtControl.egress_ctl2.lhcb_alice == 1);
//}
//

void MAPMTFEB::setDaqReset(bool flag) {
    mapmtControl.feb.resets.daq = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getDaqReset() {
    return (mapmtControl.feb.resets.daq == 1);
}

void MAPMTFEB::setDummyIngressReset(bool flag) {
    mapmtControl.feb.resets.dummy_ingress = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getDummyIngressReset() {
    return (mapmtControl.feb.resets.dummy_ingress == 1);
}

void MAPMTFEB::setGate(bool flag) {
    mapmtControl.feb.resets.gate = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getGate() {
    return (mapmtControl.feb.resets.gate == 1);
}

void MAPMTFEB::setSourceMacAddress(int mac[]) {
    // size_t len;  // TBD - not needed
    // int *array;  // not needed

// Copy Java array to MAC address array

    // len = env->GetArrayLength( mac );
    // array = env->GetIntArrayElements( mac, 0 );

    for (int i = 0; i < 6; i++) {
        // mapmtControl.feb.macsrc.uc[i] = array[i] & 0xff;
        mapmtControl.feb.macsrc.uc[i] = mac[i] & 0xff;
    }

    // env->ReleaseIntArrayElements( mac, array, 0 );

    return;
}

void MAPMTFEB::getSourceMacAddress(int mac[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy MAC address array to the Java array

    // len = env->GetArrayLength( mac );
    // array = env->GetIntArrayElements( mac, 0 );

    for (int i = 0; i < 6; i++) {
        // array[i] = mapmtControl.feb.macsrc.uc[i];
        mac[i] = mapmtControl.feb.macsrc.uc[i];
    }

    // env->ReleaseIntArrayElements( mac, array, 0 );

    return;
}

void MAPMTFEB::setDestinationMacAddress(int mac[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy Java array to MAC address array

    // len = env->GetArrayLength( mac );
    // array = env->GetIntArrayElements( mac, 0 );

    for (int i = 0; i < 6; i++) {
        // mapmtControl.feb.macdst.uc[i] = array[i] & 0xff;
        mapmtControl.feb.macdst.uc[i] = mac[i] & 0xff;
    }

    // env->ReleaseIntArrayElements( mac, array, 0 );

    return;
}

void MAPMTFEB::getDestinationMacAddress(int mac[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy MAC address array to the Java array

    // len = env->GetArrayLength( mac );
    // array = env->GetIntArrayElements( mac, 0 );

    for (int i = 0; i < 6; i++) {
        // array[i] = mapmtControl.feb.macdst.uc[i];
        mac[i] = mapmtControl.feb.macdst.uc[i];
    }

    // env->ReleaseIntArrayElements( mac, array, 0 );

    return;
}

void MAPMTFEB::setSourceIpAddress(int ip[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy Java array to IP address array

    // len = env->GetArrayLength( ip );
    // array = env->GetIntArrayElements( ip, 0 );

    for (int i = 0; i < 4; i++) {
        // mapmtControl.feb.ipsrc.uc[i] = array[i] & 0xff;
        // mapmtControl.feb.ipsrc.uc[i] = ip[i] & 0xff;
        mapmtControl.feb.ipsrc.uc[i] = ip[i];
    }

    // env->ReleaseIntArrayElements( ip, array, 0 );

    return;
}

void MAPMTFEB::getSourceIpAddress(int ip[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy IP address array to the Java array

    // len = env->GetArrayLength( ip );
    // array = env->GetIntArrayElements( ip, 0 );

    for (int i = 0; i < 4; i++) {
        // array[i] = mapmtControl.feb.ipsrc.uc[i];
        ip[i] = mapmtControl.feb.ipsrc.uc[i];
    }

    // env->ReleaseIntArrayElements( ip, array, 0 );

    return;
}

void MAPMTFEB::setDestinationIpAddress(int ip[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy Java array to IP address array

    // len = env->GetArrayLength( ip );
    // array = env->GetIntArrayElements( ip, 0 );

    for (int i = 0; i < 4; i++) {
        // mapmtControl.feb.ipdst.uc[i] = array[i] & 0xff;
        // mapmtControl.feb.ipdst.uc[i] = ip[i] & 0xff;
        mapmtControl.feb.ipdst.uc[i] = ip[i];
    }

    // env->ReleaseIntArrayElements( ip, array, 0 );

    return;
}

void MAPMTFEB::getDestinationIpAddress(int ip[]) {
    // size_t len;  // not needed
    // int *array;  // not needed

// Copy IP address array to the Java array

    // len = env->GetArrayLength( ip );
    // array = env->GetIntArrayElements( ip, 0 );

    for (int i = 0; i < 4; i++) {
        // array[i] = mapmtControl.feb.ipdst.uc[i];
        ip[i] = mapmtControl.feb.ipdst.uc[i];
    }

    // env->ReleaseIntArrayElements( ip, array, 0 );

    return;
}

bool MAPMTFEB::getSkipSmb() {
    return (mapmtControl.feb.fe_ctl.skip_smb == 1);
}

void MAPMTFEB::setSkipSmb(bool flag) {
    mapmtControl.feb.fe_ctl.skip_smb = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getTestPulseEnable() {
    return (mapmtControl.bbctl.tpen == 1);
}

void MAPMTFEB::setTestPulseEnable(bool flag) {
    mapmtControl.bbctl.tpen = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getMarCmuxrpt(int mar) {
    if ( mar == 1 ) return (mapmtControl.fe.maroc.mar1_cmuxrpt == 1);
    if ( mar == 2 ) return (mapmtControl.fe.maroc.mar2_cmuxrpt == 1);
    return false;
}

void MAPMTFEB::setMarCmuxrpt(bool flag, int mar) {
    if ( mar == 1 ) {
        mapmtControl.fe.maroc.mar1_cmuxrpt = flag ? 1 : 0;
        return;
    }
    if ( mar == 2 ) {
        mapmtControl.fe.maroc.mar2_cmuxrpt = flag ? 1 : 0;
        return;
    }
    return;
}

bool MAPMTFEB::getMarAdcEnable(int mar) {
    if ( mar == 1 ) return (mapmtControl.fe.maroc.mar1_adc_enable == 1);
    if ( mar == 2 ) return (mapmtControl.fe.maroc.mar2_adc_enable == 1);
    return false;
}

void MAPMTFEB::setMarAdcEnable(int mar, bool flag) {
    if ( mar == 1 ) {
        mapmtControl.fe.maroc.mar1_adc_enable = flag ? 1 : 0;
        return;
    }
    if ( mar == 2 ) {
        mapmtControl.fe.maroc.mar2_adc_enable = flag ? 1 : 0;
        return;
    }
    return;
}

int MAPMTFEB::getMarCmuxStop() {
    return mapmtControl.fe.maroc.cmux_stop;
}

void MAPMTFEB::setMarCmuxStop(int ch) {
    mapmtControl.fe.maroc.cmux_stop = ch & 0xff;
}

int MAPMTFEB::getMarHoldDelay() {
    return mapmtControl.fe.maroc.hold_delay;
}

void MAPMTFEB::setMarHoldDelay(int delay) {
    mapmtControl.fe.maroc.hold_delay = delay & 0xff;
}

bool MAPMTFEB::getMarSkipCfg(int mar) {
    if ( mar == 1 ) return (mapmtControl.fe.maroc.mar1_skip_cfg == 1);
    if ( mar == 2 ) return (mapmtControl.fe.maroc.mar2_skip_cfg == 1);
    return false;
}

void MAPMTFEB::setMarSkipCfg(bool flag, int mar) {
    if ( mar == 1 ) {
        mapmtControl.fe.maroc.mar1_skip_cfg = flag ? 1 : 0;
        return;
    }
    if ( mar == 2 ) {
        mapmtControl.fe.maroc.mar2_skip_cfg = flag ? 1 : 0;
        return;
    }
    return;
}

void MAPMTFEB::setStrobeLength(int strobeLength) {
    mapmtControl.feb.fe_ctl.strobe_length = strobeLength;
    return;
}

int MAPMTFEB::getStrobeLength() {
    return (mapmtControl.feb.fe_ctl.strobe_length);
}

void MAPMTFEB::setInvertFE(bool flag) {
    mapmtControl.feb.fe_ctl.invert_fe = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getInvertFE() {
    return (mapmtControl.feb.fe_ctl.invert_fe == 1);
}

void MAPMTFEB::setEnableMIIM(bool flag) {
    mapmtControl.feb.fe_ctl.enable_miim = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getEnableMIIM() {
    return(mapmtControl.feb.fe_ctl.enable_miim == 1);
}

void MAPMTFEB::setSourcesComplement(int sources) {
    mapmtControl.feb.fe_ctl.sources_complement = sources & 0xff;
}

int MAPMTFEB::getSourcesComplement() {
    return  mapmtControl.feb.fe_ctl.sources_complement;
}

void MAPMTFEB::setSourcesEnable(int sources) {
    mapmtControl.feb.fe_ctl.sources_enable = sources & 0xff;
}

int MAPMTFEB::getSourcesEnable() {
    return  mapmtControl.feb.fe_ctl.sources_enable;
}

void MAPMTFEB::setSourcesTrigger(int sources) {
    mapmtControl.feb.fe_ctl.sources_trigger = sources & 0xff;
}

int MAPMTFEB::getSourcesTrigger() {
    return  mapmtControl.feb.fe_ctl.sources_trigger;
}

void MAPMTFEB::setIpProtocol(int proto) {
    mapmtControl.feb.ip_protocol = proto;
    return;
}

int MAPMTFEB::getIpProtocol() {
    return (mapmtControl.feb.ip_protocol);
}

void MAPMTFEB::setTag(int tag) {
    mapmtControl.feb.tag = tag;
    return;
}

int MAPMTFEB::getTag() {
    return(mapmtControl.feb.tag);
}

void MAPMTFEB::setIpTtl(int ttl) {
    mapmtControl.feb.ip_ttl = ttl;
    return;
}

int MAPMTFEB::getIpTtl() {
    return (mapmtControl.feb.ip_ttl);
}

void MAPMTFEB::setMepFixedDestination(bool flag) {
    mapmtControl.feb.egress_ctl.mep_static_destination = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getMepFixedDestination() {
    return (mapmtControl.feb.egress_ctl.mep_static_destination == 1);
}

void MAPMTFEB::setMepTfcControl(bool flag) {
    mapmtControl.feb.egress_ctl.mep_no_tfc = flag ? 0 : 1;
    return;
}

bool MAPMTFEB::getMepTfcControl() {
    return (mapmtControl.feb.egress_ctl.mep_no_tfc == 0);
}

void MAPMTFEB::setMepEventCount(int count) {
    mapmtControl.feb.mep_event_count = count;
    return;
}

int MAPMTFEB::getMepEventCount() {
    return (mapmtControl.feb.mep_event_count);
}

void MAPMTFEB::setThrottleAEnable(bool flag) {
    mapmtControl.feb.egress_ctl.tfc_throttle_0 = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getThrottleAEnable() {
    return (mapmtControl.feb.egress_ctl.tfc_throttle_0 == 1);
}

void MAPMTFEB::setThrottleBEnable(bool flag) {
    mapmtControl.feb.egress_ctl.tfc_throttle_1 = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getThrottleBEnable() {
    return (mapmtControl.feb.egress_ctl.tfc_throttle_1 == 1);
}

void MAPMTFEB::setInvertedThrottleAPolarity(bool flag) {
    mapmtControl.feb.egress_ctl.tfc_throttle_0_polarity = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getInvertedThrottleAPolarity() {
    return (mapmtControl.feb.egress_ctl.tfc_throttle_0_polarity == 1);
}

void MAPMTFEB::setInvertedThrottleBPolarity(bool flag) {
    mapmtControl.feb.egress_ctl.tfc_throttle_1_polarity = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getInvertedThrottleBPolarity() {
    return (mapmtControl.feb.egress_ctl.tfc_throttle_1_polarity == 1);
}

void MAPMTFEB::setTfcDecoderEnable(bool flag) {
    mapmtControl.feb.egress_ctl.tfc_decoder_reset_b = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getTfcDecoderEnable() {
    return (mapmtControl.feb.egress_ctl.tfc_decoder_reset_b == 1);
}

void MAPMTFEB::setGbePollEnable(bool flag) {
    mapmtControl.feb.egress_ctl.gbe_poll_enable = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getGbePollEnable() {
    return (mapmtControl.feb.egress_ctl.gbe_poll_enable == 1);
}

void MAPMTFEB::setGbePort(int port) {
    mapmtControl.feb.egress_ctl.gbe_port = port;
    return;
}

int MAPMTFEB::getGbePort() {
    return (mapmtControl.feb.egress_ctl.gbe_port);
}

void MAPMTFEB::setGbeFixedPort(bool flag) {
    mapmtControl.feb.egress_ctl.gbe_static_port = flag ? 1 : 0;
    return;
}

bool MAPMTFEB::getGbeFixedPort() {
    return (mapmtControl.feb.egress_ctl.gbe_static_port == 1);
}

void MAPMTFEB::setPartitionId(int tagd) {
    mapmtControl.feb.tag = tagd;
    return;
}

int MAPMTFEB::getPartitionId() {
    return (mapmtControl.feb.tag);
}

void MAPMTFEB::setLatencyCompensation(int latency) {
    mapmtControl.feb.latency = latency;
    return;
}

int MAPMTFEB::getLatencyCompensation() {
    return (mapmtControl.feb.latency);
}

void MAPMTFEB::setL1Id(int l1id) {
    mapmtControl.feb.l1_id = l1id;
    return;
}

int MAPMTFEB::getL1Id() {
    return (mapmtControl.feb.l1_id);
}

// void MAPMTFEB::setIngressInhibit (, jint inhb)
// {
//   mapmtControl.ingress_ctl.inhibit = inhb;
//   return;
// }

// jint getIngressInhibit ()
// {
//   return (mapmtControl.ingress_ctl.inhibit);
// }

void MAPMTFEB::setDummyIngressSize(int size) {
    mapmtControl.feb.dummy_size = size;
    return;
}

int MAPMTFEB::getDummyIngressSize() {
    return (mapmtControl.feb.dummy_size);
}

void MAPMTFEB::setTruncationHwm(int hwm) {
    mapmtControl.feb.mep_hwm = hwm;
    return;
}

int MAPMTFEB::getTruncationHwm() {
    return (mapmtControl.feb.mep_hwm);
}

void MAPMTFEB::setPulseCount(int count) {
    mapmtControl.feb.pulse_count = count;
    return;
}

int MAPMTFEB::getPulseCount() {
    return (mapmtControl.feb.pulse_count);
}

void MAPMTFEB::setPulseDelay(int delay) {
    mapmtControl.feb.pulse_delay = delay;
    return;
}

int MAPMTFEB::getPulseDelay() {
    return (mapmtControl.feb.pulse_delay);
}

void MAPMTFEB::setLatency(int latency) {
    mapmtControl.feb.latency = latency;
    return;
}

int MAPMTFEB::getLatency() {
    return (mapmtControl.feb.latency);
}

void MAPMTFEB::setTriggerDelay(int delay) {
    mapmtControl.feb.trigger_delay = delay;
    return;
}

int MAPMTFEB::getTriggerDelay() {
    return (mapmtControl.feb.trigger_delay);
}

/*

jboolean isReset ()
{
    return (mapmtfebStatus.reset == 1);
}

jboolean isReady ()
{
    return (mapmtfebStatus.ready_b == 0);
}

jboolean isUpDCMLocked ()
{
    return (mapmtfebStatus.locked_up == 1);
}

jboolean isDownDCMLocked ()
{
    return (mapmtfebStatus.locked_down == 1);
}

jboolean isDCMsLocked ()
{
    return (mapmtfebStatus.dcms_ready_b == 0);
}

jboolean isTfcFifoReady ()
{
    return (mapmtfebStatus.tfcfifo_ready == 1);
}

jboolean isTfcFifoFull ()
{
    return (mapmtfebStatus.tfcfifo_full == 1);
}

jboolean isRxSignalDetect ()
{
    return (mapmtfebStatus.rxsd == 1);
}

jint getL0MuxWriteAddress ()
{
    int addr = mapmtfebStatus.l0MuxWriteAddress;
    return addr;
}

jint getL0MuxReadAddress ()
{
    int addr = mapmtfebStatus.l0MuxReadAddress;
    return addr;
}

jint getL0MuxTfcFifoWriteAddress ()
{
    int addr = mapmtfebStatus.l0MuxTfcFifoWriteAddress;
    return addr;
}

jint getL0MuxTfcFifoReadAddress ()
{
    int addr = mapmtfebStatus.l0MuxTfcFifoReadAddress;
    return addr;
}

jint getL0MuxOccupancy ()
{
    int occ = mapmtfebStatus.l0MuxOccupancy;
    return occ;
}

jint getL0MuxTfcFifoOccupancy ()
{
    int occ = mapmtfebStatus.l0MuxTfcFifoOccupancy;
    return occ;
}

jboolean isTfcFifoDataRequestAsserted ()
{
    return (mapmtfebStatus.tfcFifoRequest == 1);
}

jboolean isTtcReady ()
{
    return (mapmtfebStatus.ttcReady == 1);
}

jboolean isSpi3TxBusy ()
{
    return (mapmtfebStatus.spi3TxBusy == 1);
}

jboolean isMepFifoFull ()
{
    return (mapmtfebStatus.mepFifoFull == 1);
}

jboolean isThrottleAsserted ()
{
    return (mapmtfebStatus.throttleOr == 1);
}

jboolean isTfcL0ResetAsserted ()
{
    return (mapmtfebStatus.tfcL0Reset == 1);
}

jboolean isTfcL1ResetAsserted ()
{
    return (mapmtfebStatus.tfcL1Reset == 1);
}

jint getTfcBShort ()
{
    int bshort = mapmtfebStatus.tfcBShort;
    return bshort;
}

jint getTpa ()
{
    int tpa = mapmtfebStatus.tpa;
    return tpa;
}

jboolean isFragmentReady ()
{
    return (mapmtfebStatus.fragmentReady == 1);
}

jboolean isMepReady ()
{
    return (mapmtfebStatus.mepReady == 1);
}

jboolean isFragmentRequestAsserted ()
{
    return (mapmtfebStatus.fragmentReq == 1);
}

jboolean isEventQValidAsserted ()
{
    return (mapmtfebStatus.evtQValid == 1);
}

jboolean isEthernetQValidAsserted ()
{
    return (mapmtfebStatus.ethQValid == 1);
}

jboolean isTfcSequenceErrorAsserted ()
{
    return (mapmtfebStatus.aSequenceError == 1);
}

jint getTfcBLong ()
{
    int blong = mapmtfebStatus.tfcBLong;
    return blong;
}

jint getMepFifoOccupancy ()
{
    int occ = mapmtfebStatus.mepCount;
    return occ;
}

jint getTfcFifoOccupancy ()
{
    int occ = mapmtfebStatus.tfcFifoOccupancy;
    return occ;
}

jint getTfcFifoInCount ()
{
    int count = mapmtfebStatus.tfcFifoCountIn;
    return count;
}

jint getTfcFifoOutCount ()
{
    int count = mapmtfebStatus.tfcFifoCountOut;
    return count;
}

jint getE100RxInCount ()
{
    int count = mapmtfebStatus.e100RxCountIn;
    return count;
}

jint getE100RxOutCount ()
{
    int count = mapmtfebStatus.e100RxCountOut;
    return count;
}

jint getE100TxInCount ()
{
    int count = mapmtfebStatus.e100TxCountIn;
    return count;
}

jint getE100TxOutCount ()
{
    int count = mapmtfebStatus.e100TxCountOut;
    return count;
}

jint getParityErrorCount (, jint i, jint ch)
{
     int count = channelStatus[int(i)][int(ch)].rx_disperr_count;
     return count;
}

jint getIngressCount(, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].ingress_count;
    return count;
}

jint getCosynchroCount (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].cosync_count;
    return count;
}

jint getClockCorrectionCount (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].rx_clkcor_count;
    return count;
}

jint getBufferErrorCount (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].rx_buferr_count;
    return count;
}

jint getZSCount (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].zs_count;
    return count;
}

jint getZSFifoReadAddress (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].zsfifo_out_count;
    return count;
}

jint getZSFifoWriteAddress (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].zsfifo_in_count;
    return count;
}

jint getZSA (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].zs_a_count;
    return count;
}

jint getZSB (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].zs_b_count;
    return count;
}

jint getL0TimeOfArrival (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].latency_l0;
    return count;
}

jint getTfcTimeOfArrival (, jint i, jint ch)
{
    int count = channelStatus[int(i)][int(ch)].latency_tfc;
    return count;
}

jint getRxLos (, jint i, jint ch)
{
    int state = channelStatus[int(i)][int(ch)].status.rx_los;
    return state;
}

jboolean getRxBufferHalfFull (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.rxbuffer_half_full == 1);
}

jboolean getRxBufferOverflow (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.rxbuffer_overflow == 1);
}

jboolean getGtInhibit (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.gt_inhibit == 1);
}

jboolean getCosynchroError (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.cosync_error == 1);
}

jboolean getIngressBufferReady (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.ingress_buffer_ready == 1);
}

jboolean getIngressBufferFull (, jint i, jint ch)
{
    return (channelStatus[int(i)][int(ch)].status.ingress_buffer_full == 1);
}

*/
