/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/


#include <stdint.h>
#include <string>

#include <iostream>

#include "../include/custom.hpp"
#include "../include/maroc3.hpp"

#include "../include/maroc3csr.h"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"

// using byte = uint8_t;  // define byte type

// maroc3Control_u_t m3[2];
// maroc3SumGain_u_t m3sg[64];
// unsigned maroc3MaskOr[4];

MAROC3::MAROC3() : m3(), m3sg(), maroc3MaskOr() {}

int MAROC3::setStateFromPreferences(tinyxml2::XMLDocument *doc, tinyxml2::XMLDocument *marocPD, int32_t db) {
    int32_t curr_db, pI, ch, asic, gain;
    bool b, mask_or1, mask_or2, sum, ctest;

    tinyxml2::XMLNode * node = getNodeByName(doc, "default");  // go to section in XML for settings
    tinyxml2::XMLNode * marocNode = getNodeByName(marocPD, "PD");  // go to section in XML for mapping
    if (node == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC for settings" << std::endl;
        return -1;
    }

    if (marocNode == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC for mapping" << std::endl;
        return -1;
    }

    bool global_set[2] = {true, true};

    // for (int i = 0; i < 2; i++) {  // [BUG] This loop is not needed
        // if (i != db) continue;
        // std::string sections = "ABCD";
        std::string section;
        for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
            // ORIENTATION - to be adjusted
            if (k == 0) {
                section = "A";
            } else if (k == 1) {
                section = "B";
            } else if (k == 2) {
                section = "C";
            } else {
                section = "D";
            }

            node = moveToNode2(node, section);
            if (node == NULL) {
                std::cerr << "ERROR: Moving node to " << section.c_str()
                << " in general maroc settings failed. Aborting..." << std::endl;
                return -1;
            }

            marocNode = moveToNode2(marocNode, section);
            if (marocNode == NULL) {
                std::cerr << "ERROR: Moving marocNode to " << section.c_str()
                << " in maroc channel settings failed. Aborting..." << std::endl;
                return -1;
            }

            tinyxml2::XMLNode * tempnode = node;  // temp node for traversing
            tinyxml2::XMLNode * tempMarocNode = marocNode;  // temp marocNode for traversing

            for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
                tempnode = node;
                tempMarocNode = marocNode;

                std::stringstream anode_name;
                if (j+1 < 10) {
                    anode_name << 0 << j+1;
                } else {
                    anode_name << j+1;
                }

                tempnode = moveToInnerNode2(tempnode, anode_name.str());
                if (tempnode == NULL) {
                    std::cerr << "ERROR: Getting maroc settings for PD Anode " << anode_name.str().c_str()
                    << " failed. Aborting..." << std::endl;
                    return -1;
                }

                tempMarocNode = moveToInnerNode2(tempMarocNode, anode_name.str());
                if (tempMarocNode == NULL) {
                    std::cerr << "ERROR: Moving tempMarocNode to " << anode_name.str().c_str()
                    << " in maroc channel settings failed. Aborting..." << std::endl;
                    return -1;
                }

                // int ch, asic, db;
                getIntPropertyFromXMLNode2(tempMarocNode, "db", &curr_db, 0);
                if (curr_db != db) {
                    // printf("DB mismatch on anode %s in section %s\n", anode_name.str().c_str(), section.c_str());
                    anode_name.str(std::string());
                    continue;
                }

                getIntPropertyFromXMLNode2(tempMarocNode, "channel", &ch, 0);
                getIntPropertyFromXMLNode2(tempMarocNode, "asic", &asic, 0);

                int debug = 0;
                if (debug) {
                    std::cerr << "Node: " << node->ToElement()->FirstAttribute()->Value()
                    << "; Tempnode: " << tempnode->ToElement()->FirstAttribute()->Value() << ";" << std::endl;
                    std::cerr << "MarocNode: " << marocNode->ToElement()->FirstAttribute()->Value()
                    << "; TempMarocnode: " << tempMarocNode->ToElement()->FirstAttribute()->Value() << ";" << std::endl;
                    std::cerr << "curr_db: " << curr_db << "; db: " << db << ";" << std::endl;
                    std::cerr << "ch: " << ch << "; j: " << j << ";" << std::endl;
                    std::cerr << "asic: " << asic << "; [j,k]: [" << j << "," << k << "];" << std::endl;
                    // std::cerr << "asic: " << asic << "; [i,k,j]: [" << i << "," << k << "," << j << "];" << std::endl;
                }

                if (global_set[asic]) {
                    global_set[asic] = false;
                    // bool b;
                    getBoolPropertyFromXMLNode2(node, "maroc.small_dac", &b, false);
                    set_small_dac(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.small_dac", &b, false);
                    set_small_dac(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.enb_outADC", &b, false);
                    set_enb_outADC(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.inv_startCmptGray", &b, false);
                    set_inv_startCmptGray(b?1:0, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.ramp_8bit", &b, false);
                    set_ramp_8bit(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.ramp_10bit", &b, false);
                    set_ramp_10bit(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.cmd_CK_mux", &b, false);
                    set_cmd_CK_mux(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.d1_d2", &b, false);
                    set_d1_d2(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.inv_discriADC", &b, false);
                    set_inv_discriADC(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.polar_discri", &b, false);
                    set_polar_discri(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.enb_tristate", &b, false);
                    set_enb_tristate(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.valid_dc_fsb2", &b, false);
                    set_valid_dc_fsb2(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb2_50f", &b, false);
                    set_sw_fsb2_50f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb2_100f", &b, false);
                    set_sw_fsb2_100f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb2_100k", &b, false);
                    set_sw_fsb2_100k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb2_50k", &b, false);
                    set_sw_fsb2_50k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.valid_dc_fs", &b, false);
                    set_valid_dc_fs(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.cmd_fsb_fsu", &b, false);
                    set_cmd_fsb_fsu(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb1_50f", &b, false);
                    set_sw_fsb1_50f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb1_100f", &b, false);
                    set_sw_fsb1_100f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb1_100k", &b, false);
                    set_sw_fsb1_100k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsb1_50k", &b, false);
                    set_sw_fsb1_50k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsu_100k", &b, false);
                    set_sw_fsu_100k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsu_50k", &b, false);
                    set_sw_fsu_50k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsu_25k", &b, false);
                    set_sw_fsu_25k(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsu_40f", &b, false);
                    set_sw_fsu_40f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_fsu_20f", &b, false);
                    set_sw_fsu_20f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.h1h2_choice", &b, false);
                    set_h1h2_choice(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.en_adc", &b, false);
                    set_en_adc(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_ss_1200f", &b, false);
                    set_sw_ss_1200f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_ss_600f", &b, false);
                    set_sw_ss_600f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.sw_ss_300f", &b, false);
                    set_sw_ss_300f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.onoff_ss", &b, false);
                    set_onoff_ss(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.swb_buf_2p", &b, false);
                    set_swb_buf_2p(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.swb_buf_1p", &b, false);
                    set_swb_buf_1p(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.swb_buf_500f", &b, false);
                    set_swb_buf_500f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.swb_buf_250f", &b, false);
                    set_swb_buf_250f(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.cmd_fsb", &b, false);
                    set_cmd_fsb(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.cmd_ss", &b, false);
                    set_cmd_ss(b, asic);
                    getBoolPropertyFromXMLNode2(node, "maroc.cmd_fsu", &b, false);
                    set_cmd_fsu(b, asic);
                    getIntPropertyFromXMLNode2(node, "maroc.dac.0", &pI, 0x80);
                    set_dac(0, pI, asic);
                    getIntPropertyFromXMLNode2(node, "maroc.dac.1", &pI, 0x80);
                    set_dac(1, pI, asic);
                }

                // get preferences

                getIntPropertyFromXMLNode2(tempnode, "maroc.gain", &gain, 128);
                set_gain(ch, gain, asic);
                getBoolPropertyFromXMLNode2(tempnode, "maroc.sum", &sum, false);
                set_cmd_sum(ch, sum?1:0, asic);
                getBoolPropertyFromXMLNode2(tempnode, "maroc.ctest", &ctest, false);
                set_ctest(ch, ctest?1:0, asic);
                getBoolPropertyFromXMLNode2(tempnode, "maroc.mask-or1", &mask_or1, false);
                set_mask_or1(ch, mask_or1?1:0, asic);
                getBoolPropertyFromXMLNode2(tempnode, "maroc.mask-or2", &mask_or2, false);
                set_mask_or2(ch, mask_or2?1:0, asic);

                anode_name.str(std::string());
            }
        }
    // }
    return 0;
}

int MAROC3::setGlobalGain(tinyxml2::XMLDocument *marocPD, pmt_t pmt[], int32_t db) {
    int32_t curr_db, ch, asic, gain;

    tinyxml2::XMLNode * marocNode = getNodeByName(marocPD, "PD");  // go to section in XML for mapping

    if (marocNode == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC mapping" << std::endl;
        return -1;
    }

    // for (int i = 0; i < 2; i++) {  // [BUG] This loop is not needed
        // if (i != db) continue;

        // std::string sections = "ABCD";
        std::string section;
        for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
            // ORIENTATION - to be adjusted
            if (k == 0) {
                section = "A";
            } else if (k == 1) {
                section = "B";
            } else if (k == 2) {
                section = "C";
            } else {
                section = "D";
            }

            marocNode = moveToNode2(marocNode, section);
            if (marocNode == NULL) {
                std::cerr << "ERROR: Moving marocNode to " << section.c_str()
                << " in maroc channel settings failed. Aborting..." << std::endl;
                return -1;
            }

            tinyxml2::XMLNode * tempMarocNode = marocNode;  // temp marocNode for traversing

            for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
                tempMarocNode = marocNode;

                std::stringstream anode_name;
                if (j+1 < 10) {
                    anode_name << 0 << j+1;
                } else {
                    anode_name << j+1;
                }

                tempMarocNode = moveToInnerNode2(tempMarocNode, anode_name.str());
                if (tempMarocNode == NULL) {
                    std::cerr << "ERROR: Moving tempMarocNode to " << anode_name.str().c_str()
                    << " in maroc channel settings failed. Aborting..." << std::endl;
                    return -1;
                }

                // int ch, asic, db;
                getIntPropertyFromXMLNode2(tempMarocNode, "db", &curr_db, 0);
                if (curr_db != db) {
                    // printf("DB mismatch on anode %s in section %s\n", anode_name.str().c_str(), section.c_str());
                    anode_name.str(std::string());
                    continue;
                }

                getIntPropertyFromXMLNode2(tempMarocNode, "channel", &ch, 0);
                getIntPropertyFromXMLNode2(tempMarocNode, "asic", &asic, 0);

                int debug = 0;
                if (debug) {
                    std::cerr << "MarocNode: " << marocNode->ToElement()->FirstAttribute()->Value()
                    << "; TempMarocnode: " << tempMarocNode->ToElement()->FirstAttribute()->Value() << ";" << std::endl;
                    std::cerr << "curr_db: " << curr_db << "; db: " << db << ";" << std::endl;
                    std::cerr << "ch: " << ch << "; j: " << j << ";" << std::endl;
                    std::cerr << "asic: " << asic << "; [j,k]: [" << j << "," << k << "];" << std::endl;
                    // std::cerr << "asic: " << asic << "; [i,k,j]: [" << i << "," << k << "," << j << "];" << std::endl;
                }

                // set gain

                // gain = pmt[asic].gain;
                gain = pmt[(k == 0 || k == 2) ? 0 : 1].gain;
                set_gain(ch, gain, asic);

                anode_name.str(std::string());
            }
        }
    // }
    return 0;
}

int MAROC3::setGlobalCTest(tinyxml2::XMLDocument *marocPD, bool testpulse, int32_t db) {
    int32_t curr_db, ch, asic;

    tinyxml2::XMLNode * marocNode = getNodeByName(marocPD, "PD");  // go to section in XML for mapping

    if (marocNode == NULL) {
        std::cerr << "ERROR: Failed to read XML doc for MAROC mapping" << std::endl;
        return -1;
    }

    // for (int i = 0; i < 2; i++) {  // [BUG] This loop is not needed
        // if (i != db) continue;

        // std::string sections = "ABCD";
        std::string section;
        for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
            // ORIENTATION - to be adjusted
            if (k == 0) {
                section = "A";
            } else if (k == 1) {
                section = "B";
            } else if (k == 2) {
                section = "C";
            } else {
                section = "D";
            }

            marocNode = moveToNode2(marocNode, section);
            if (marocNode == NULL) {
                std::cerr << "ERROR: Moving marocNode to " << section.c_str()
                << " in maroc channel settings failed. Aborting..." << std::endl;
                return -1;
            }

            tinyxml2::XMLNode * tempMarocNode = marocNode;  // temp marocNode for traversing

            for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
                tempMarocNode = marocNode;

                std::stringstream anode_name;
                if (j+1 < 10) {
                    anode_name << 0 << j+1;
                } else {
                    anode_name << j+1;
                }

                tempMarocNode = moveToInnerNode2(tempMarocNode, anode_name.str());
                if (tempMarocNode == NULL) {
                    std::cerr << "ERROR: Moving tempMarocNode to " << anode_name.str().c_str()
                    << " in maroc channel settings failed. Aborting..." << std::endl;
                    return -1;
                }

                // int ch, asic, db;
                getIntPropertyFromXMLNode2(tempMarocNode, "db", &curr_db, 0);
                if (curr_db != db) {
                    // printf("DB mismatch on anode %s in section %s\n", anode_name.str().c_str(), section.c_str());
                    anode_name.str(std::string());
                    continue;
                }

                getIntPropertyFromXMLNode2(tempMarocNode, "channel", &ch, 0);
                getIntPropertyFromXMLNode2(tempMarocNode, "asic", &asic, 0);

                int debug = 0;
                if (debug) {
                    std::cerr << "MarocNode: " << marocNode->ToElement()->FirstAttribute()->Value()
                    << "; TempMarocnode: " << tempMarocNode->ToElement()->FirstAttribute()->Value() << ";" << std::endl;
                    std::cerr << "curr_db: " << curr_db << "; db: " << db << ";" << std::endl;
                    std::cerr << "ch: " << ch << "; j: " << j << ";" << std::endl;
                    std::cerr << "asic: " << asic << "; [j,k]: [" << j << "," << k << "];" << std::endl;
                    // std::cerr << "asic: " << asic << "; [i,k,j]: [" << i << "," << k << "," << j << "];" << std::endl;
                }

                // set ctest

                bool ctest = testpulse;
                set_ctest(ch, ctest?1:0, asic);

                anode_name.str(std::string());
            }
        }
    // }
    return 0;
}

// int MAROC3::setStateFromPreferences(tinyxml2::XMLDocument * doc, tinyxml2::XMLDocument * marocPD) {
//     tinyxml2::XMLNode * node = getNodeByName(doc, "default");  // go to section in XML
//     tinyxml2::XMLNode * marocNode = getNodeByName(marocPD, "PD");  // go to section in XML
//     if (node == NULL) {
//         std::cerr << "ERROR: Failed to read XML doc for MAROC" << std::endl;
//         return -1;
//     }

//     for (int i = 0; i < 2; i++) {  // 2 MAROCs per chim
//         // std::string sections = "ABCD";
//         std::string section;
//         for (int k = 0; k < 4; k++) {  // four sectors A(0),B(0),C(1),D(1)
//             // ORIENTATION - to be adjusted
//             if (k == 0) {
//                 section = "A";
//             } else if (k == 1) {
//                 section = "B";
//             } else if (k == 2) {
//                 section = "C";
//             } else {
//                 section = "D";
//             }
//             if (moveToNode(node, section) != 0) {
//                 std::cerr << "ERROR: Moving node to " << section.c_str()
//                 << " in general maroc settings failed. Aborting..." << std::endl;
//                 return -1;
//             }
//             if (moveToNode(marocNode, section) != 0) {
//                 std::cerr << "ERROR: Moving marocNode to " << section.c_str()
//                 << " in maroc channel settings failed. Aborting..." << std::endl;
//                 return -1;
//             }

//             tinyxml2::XMLNode * tempnode = node;  // temp node for traversing
//             tinyxml2::XMLNode * tempMarocNode = marocNode;  // temp marocNode for traversing
//             for (int j = 0; j < 64; j++) {  // 64 channels per MAROC
//                 tempnode = node;
//                 tempMarocNode = marocNode;

//                 std::stringstream anode_name;
//                 if (j+1 < 10) {
//                     anode_name << 0 << j+1;
//                 } else {
//                     anode_name << j+1;
//                 }

//                 if (moveToInnerNode(tempMarocNode, anode_name.str()) != 0) {
//                     std::cerr << "ERROR: Moving tempNode to " << anode_name.str().c_str()
//                     << " in maroc channel settings failed. Aborting..." << std::endl;
//                     return -1;
//                 }

//                 int ch, asic, db;
//                 getIntPropertyFromXMLNode(tempMarocNode, "db", db, 0);
//                 if (db != i) {
//                     // printf("DB mismatch on anode %s in section %s\n", anode_name.str(), section.c_str());
//                     anode_name.str(std::string());
//                     continue;
//                 }
//                 getIntPropertyFromXMLNode(tempMarocNode, "channel", ch, 0);
//                 getIntPropertyFromXMLNode(tempMarocNode, "asic", asic, 0);

//                 bool b;
//                 getBoolPropertyFromXMLNode(node, "maroc.small_dac", b, false);
//                 set_small_dac(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.small_dac", b, false);
//                 set_small_dac(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.enb_outADC", b, false);
//                 set_enb_outADC(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.inv_startCmptGray", b, false);
//                 set_inv_startCmptGray(b?1:0, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.ramp_8bit", b, false);
//                 set_ramp_8bit(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.ramp_10bit", b, false);
//                 set_ramp_10bit(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.cmd_CK_mux", b, false);
//                 set_cmd_CK_mux(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.d1_d2", b, false);
//                 set_d1_d2(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.inv_discriADC", b, false);
//                 set_inv_discriADC(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.polar_discri", b, false);
//                 set_polar_discri(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.enb_tristate", b, false);
//                 set_enb_tristate(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.valid_dc_fsb2", b, false);
//                 set_valid_dc_fsb2(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb2_50f", b, false);
//                 set_sw_fsb2_50f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb2_100f", b, false);
//                 set_sw_fsb2_100f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb2_100k", b, false);
//                 set_sw_fsb2_100k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb2_50k", b, false);
//                 set_sw_fsb2_50k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.valid_dc_fs", b, false);
//                 set_valid_dc_fs(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.cmd_fsb_fsu", b, false);
//                 set_cmd_fsb_fsu(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb1_50f", b, false);
//                 set_sw_fsb1_50f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb1_100f", b, false);
//                 set_sw_fsb1_100f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb1_100k", b, false);
//                 set_sw_fsb1_100k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsb1_50k", b, false);
//                 set_sw_fsb1_50k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsu_100k", b, false);
//                 set_sw_fsu_100k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsu_50k", b, false);
//                 set_sw_fsu_50k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsu_25k", b, false);
//                 set_sw_fsu_25k(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsu_40f", b, false);
//                 set_sw_fsu_40f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_fsu_20f", b, false);
//                 set_sw_fsu_20f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.h1h2_choice", b, false);
//                 set_h1h2_choice(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.en_adc", b, false);
//                 set_en_adc(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_ss_1200f", b, false);
//                 set_sw_ss_1200f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_ss_600f", b, false);
//                 set_sw_ss_600f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.sw_ss_300f", b, false);
//                 set_sw_ss_300f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.onoff_ss", b, false);
//                 set_onoff_ss(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.swb_buf_2p", b, false);
//                 set_swb_buf_2p(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.swb_buf_1p", b, false);
//                 set_swb_buf_1p(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.swb_buf_500f", b, false);
//                 set_swb_buf_500f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.swb_buf_250f", b, false);
//                 set_swb_buf_250f(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.cmd_fsb", b, false);
//                 set_cmd_fsb(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.cmd_ss", b, false);
//                 set_cmd_ss(b, asic);
//                 getBoolPropertyFromXMLNode(node, "maroc.cmd_fsu", b, false);
//                 set_cmd_fsu(b, asic);
//                 int pI;
//                 getIntPropertyFromXMLNode(node, "maroc.dac.0", pI, 0x80);
//                 set_dac(0, pI, asic);
//                 getIntPropertyFromXMLNode(node, "maroc.dac.1", pI, 0x80);
//                 set_dac(1, pI, asic);

//                 // std::stringstream ch_name;
//                 // if (ch+1 < 10) {
//                 //     ch_name << 0 << ch+1;
//                 // } else {
//                 //     ch_name << ch+1;
//                 // }

//                 if (moveToInnerNode(tempnode, anode_name.str()) != 0) {
//                     std::cerr << "ERROR: Getting maroc settings from " << anode_name.str().c_str()
//                     << " failed. Aborting..." << std::endl;
//                     return -1;
//                 }

//                 // get preferences
//                 int gain;
//                 getIntPropertyFromXMLNode(tempnode, "maroc.gain", gain, 128);
//                 set_gain(ch, gain, asic);
//                 bool sum;
//                 getBoolPropertyFromXMLNode(tempnode, "maroc.sum", sum, false);
//                 set_cmd_sum(ch, sum, asic);
//                 bool ctest;
//                 getBoolPropertyFromXMLNode(tempnode, "maroc.ctest", ctest, false);
//                 set_ctest(ch, ctest, asic);
//                 bool mask_or1;
//                 getBoolPropertyFromXMLNode(tempnode, "maroc.mask-or1", mask_or1, false);
//                 set_mask_or1(ch, mask_or1, asic);
//                 bool mask_or2;
//                 getBoolPropertyFromXMLNode(tempnode, "maroc.mask-or2", mask_or2, false);
//                 set_mask_or2(ch, mask_or2, asic);

//                 anode_name.str(std::string());
//             }
//         }
//     }
//     return 0;
// }


// void MAROC3::setStateFromPreferences(Preferences topPrefs, int db) {  // reimplement in C++
// void MAROC3::setStateFromPreferences() {  // reimplement in C++

//     int pI;
//     boolean b;

//     // try {
//     //     topPrefs.sync();
//     // } catch (BackingStoreException ex) { ex.printStackTrace(); }

//     // System.out.println("Setting preferences from "+topPrefs.absolutePath()+" for PDMDB "+db);
//     try {
//         for ( String pd : topPrefs.childrenNames() ) {
//             for ( String anode : topPrefs.node(pd).childrenNames() ) {

//                 Preferences pdPrefs = Preferences.userRoot().node("RICH/MAROC3/PD").node(pd).node(anode);
//                 if (db != pdPrefs.getInt("db",0)) continue;
//                 int asic = pdPrefs.getInt("asic",0);
//                 int ch = pdPrefs.getInt("channel",0);

//                 Preferences anodePrefs = topPrefs.node(pd).node(anode);
//                 int gain = anodePrefs.getInt("maroc.gain",0x80);
//                 set_gain(ch,gain,asic);
//                 int sum = anodePrefs.getBoolean("maroc.sum",false)?1:0;
//                 set_cmd_sum(ch,sum,asic);
//                 int ctest = anodePrefs.getBoolean("maroc.ctest",false)?1:0;
//                 set_ctest(ch,ctest,asic);
//                 set_mask_or1(ch,anodePrefs.getBoolean("maroc.mask-or1",false)?1:0,asic);
//                 set_mask_or2(ch,anodePrefs.getBoolean("maroc.mask-or2",false)?1:0,asic);

//                 // The following are global settings

//                 boolean [] doMarocGlobal = new boolean [2];
//                 doMarocGlobal[0] = true; doMarocGlobal[1] = true;

//                 Preferences marocPrefs = topPrefs.node(pd);

//                 if ( doMarocGlobal[asic] ) {
//                     doMarocGlobal[asic] = false;
//                     b = marocPrefs.getBoolean("maroc.small_dac",false);
//                     set_small_dac(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.small_dac",false);
//                     set_small_dac(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.enb_outADC",false);
//                     set_enb_outADC(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.inv_startCmptGray",false);
//                     set_inv_startCmptGray(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.ramp_8bit",false);
//                     set_ramp_8bit(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.ramp_10bit",false);
//                     set_ramp_10bit(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.cmd_CK_mux",false);
//                     set_cmd_CK_mux(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.d1_d2",false);
//                     set_d1_d2(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.inv_discriADC",false);
//                     set_inv_discriADC(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.polar_discri",false);
//                     set_polar_discri(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.enb_tristate",false);
//                     set_enb_tristate(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.valid_dc_fsb2",false);
//                     set_valid_dc_fsb2(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb2_50f",false);
//                     set_sw_fsb2_50f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb2_100f",false);
//                     set_sw_fsb2_100f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb2_100k",false);
//                     set_sw_fsb2_100k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb2_50k",false);
//                     set_sw_fsb2_50k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.valid_dc_fs",false);
//                     set_valid_dc_fs(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.cmd_fsb_fsu",false);
//                     set_cmd_fsb_fsu(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb1_50f",false);
//                     set_sw_fsb1_50f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb1_100f",false);
//                     set_sw_fsb1_100f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb1_100k",false);
//                     set_sw_fsb1_100k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsb1_50k",false);
//                     set_sw_fsb1_50k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsu_100k",false);
//                     set_sw_fsu_100k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsu_50k",false);
//                     set_sw_fsu_50k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsu_25k",false);
//                     set_sw_fsu_25k(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsu_40f",false);
//                     set_sw_fsu_40f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_fsu_20f",false);
//                     set_sw_fsu_20f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.h1h2_choice",false);
//                     set_h1h2_choice(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.en_adc",false);
//                     set_en_adc(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_ss_1200f",false);
//                     set_sw_ss_1200f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_ss_600f",false);
//                     set_sw_ss_600f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.sw_ss_300f",false);
//                     set_sw_ss_300f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.onoff_ss",false);
//                     set_onoff_ss(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.swb_buf_2p",false);
//                     set_swb_buf_2p(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.swb_buf_1p",false);
//                     set_swb_buf_1p(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.swb_buf_500f",false);
//                     set_swb_buf_500f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.swb_buf_250f",false);
//                     set_swb_buf_250f(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.cmd_fsb",false);
//                     set_cmd_fsb(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.cmd_ss",false);
//                     set_cmd_ss(b?1:0,asic);
//                     b = marocPrefs.getBoolean("maroc.cmd_fsu",false);
//                     set_cmd_fsu(b?1:0,asic);
//                     pI = marocPrefs.getInt("maroc.dac.0",0x80);
//                     set_dac(0,pI,asic);
//                     pI = marocPrefs.getInt("maroc.dac.1",0x80);
//                     set_dac(1,pI,asic);
//                 }
//             }
//         }
//     } catch(Exception e){ e.printStackTrace(); }

//     return;
// }

void MAROC3::getRegisterArray(int mar, byte regs[]) {  // DO NOT USE THIS
    size_t len;
    // byte *array;  // not needed atm

// Copy entire register map to the Java array

    // len = env->GetArrayLength( regs );
    len = 104;  // probably 128 per board, but needs to match m3[].uc size of 104
    // array = env->GetByteArrayElements( regs, 0 );

    for (unsigned i = 0; i < len; i++) {
        // array[i] = m3[mar].uc[i];
        regs[i] = m3[mar].uc[i];
    }

    // env->ReleaseByteArrayElements( regs, array, 0 );

    return;
}

void MAROC3::getRegisterArray(int mar, byte regs[], int nreg, int off) {  // len -> LENGTH IS SAFE HERE
    size_t len;
    // byte *array;  // not needed atm

// Copy to the Java array

    // len = env->GetArrayLength( regs );
    len = bufSendSize;  // probably bufSendSize - only used for overflow safety
    // array = env->GetByteArrayElements( regs, 0 );

    for (unsigned i = 0; (i+off < len) && (i < sizeof(maroc3Control_u_t)) && (static_cast<int>(i) < nreg); i++) {
        // array[i+off] = m3[mar].uc[i];
        regs[i+off] = m3[mar].uc[i];
    }

    // env->ReleaseByteArrayElements( regs, array, 0 );

    return;
}

void MAROC3::set_small_dac(int i, int mar) {
    m3[mar].ctl.small_dac = i;
    return;
}

int MAROC3::get_small_dac(int mar) {
    return(m3[mar].ctl.small_dac);
}

void MAROC3::set_enb_outADC(int i, int mar) {
    m3[mar].ctl.enb_outADC = i;
    return;
}

void MAROC3::set_inv_startCmptGray(int i, int mar) {
    m3[mar].ctl.inv_startCmptGray = i;
    return;
}

void MAROC3::set_ramp_8bit(int i, int mar) {
    m3[mar].ctl.ramp_8bit = i;
    return;
}

void MAROC3::set_ramp_10bit(int i, int mar) {
    m3[mar].ctl.ramp_10bit = i;
    return;
}

void MAROC3::set_cmd_CK_mux(int i, int mar) {
    m3[mar].ctl.cmd_CK_mux = i;
    return;
}

void MAROC3::set_d1_d2(int i, int mar) {
    m3[mar].ctl.d1_d2 = i;
    return;
}

void MAROC3::set_inv_discriADC(int i, int mar) {
    m3[mar].ctl.inv_discriADC = i;
    return;
}

void MAROC3::set_polar_discri(int i, int mar) {
    m3[mar].ctl.polar_discri = i;
    return;
}

void MAROC3::set_enb_tristate(int i, int mar) {
    m3[mar].ctl.enb_tristate = i;
    return;
}

void MAROC3::set_valid_dc_fsb2(int i, int mar) {
    m3[mar].ctl.valid_dc_fsb2 = i;
    return;
}

void MAROC3::set_sw_fsb2_50f(int i, int mar) {
    m3[mar].ctl.sw_fsb2_50f = i;
    return;
}

void MAROC3::set_sw_fsb2_100f(int i, int mar) {
    m3[mar].ctl.sw_fsb2_100f = i;
    return;
}

void MAROC3::set_sw_fsb2_100k(int i, int mar) {
    m3[mar].ctl.sw_fsb2_100k = i;
    return;
}

void MAROC3::set_sw_fsb2_50k(int i, int mar) {
    m3[mar].ctl.sw_fsb2_50k = i;
    return;
}

void MAROC3::set_valid_dc_fs(int i, int mar) {
    m3[mar].ctl.valid_dc_fs = i;
    return;
}

void MAROC3::set_cmd_fsb_fsu(int i, int mar) {
    m3[mar].ctl.cmd_fsb_fsu = i;
    return;
}

void MAROC3::set_sw_fsb1_50f(int i, int mar) {
    m3[mar].ctl.sw_fsb1_50f = i;
    return;
}

void MAROC3::set_sw_fsb1_100f(int i, int mar) {
    m3[mar].ctl.sw_fsb1_100f = i;
    return;
}

void MAROC3::set_sw_fsb1_100k(int i, int mar) {
    m3[mar].ctl.sw_fsb1_100k = i;
    return;
}

void MAROC3::set_sw_fsb1_50k(int i, int mar) {
    m3[mar].ctl.sw_fsb1_50k = i;
    return;
}

void MAROC3::set_sw_fsu_100k(int i, int mar) {
    m3[mar].ctl.sw_fsu_100k = i;
    return;
}

void MAROC3::set_sw_fsu_50k(int i, int mar) {
    m3[mar].ctl.sw_fsu_50k = i;
    return;
}

void MAROC3::set_sw_fsu_25k(int i, int mar) {
    m3[mar].ctl.sw_fsu_25k = i;
    return;
}

void MAROC3::set_sw_fsu_40f(int i, int mar) {
    m3[mar].ctl.sw_fsu_40f = i;
    return;
}

void MAROC3::set_sw_fsu_20f(int i, int mar) {
    m3[mar].ctl.sw_fsu_20f = i;
    return;
}

void MAROC3::set_h1h2_choice(int i, int mar) {
    m3[mar].ctl.h1h2_choice = i;
    return;
}

void MAROC3::set_en_adc(int i, int mar) {
    m3[mar].ctl.en_adc = i;
    return;
}

void MAROC3::set_sw_ss_1200f(int i, int mar) {
    m3[mar].ctl.sw_ss_1200f = i;
    return;
}

void MAROC3::set_sw_ss_600f(int i, int mar) {
    m3[mar].ctl.sw_ss_600f = i;
    return;
}

void MAROC3::set_sw_ss_300f(int i, int mar) {
    m3[mar].ctl.sw_ss_300f = i;
    return;
}

void MAROC3::set_onoff_ss(int i, int mar) {
    m3[mar].ctl.onoff_ss = i;
    return;
}

void MAROC3::set_swb_buf_2p(int i, int mar) {
    m3[mar].ctl.swb_buf_2p = i;
    return;
}

void MAROC3::set_swb_buf_1p(int i, int mar) {
    m3[mar].ctl.swb_buf_1p = i;
    return;
}

void MAROC3::set_swb_buf_500f(int i, int mar) {
    m3[mar].ctl.swb_buf_500f = i;
    return;
}

void MAROC3::set_swb_buf_250f(int i, int mar) {
    m3[mar].ctl.swb_buf_250f = i;
    return;
}

void MAROC3::set_cmd_fsb(int i, int mar) {
    m3[mar].ctl.cmd_fsb = i;
    return;
}

void MAROC3::set_cmd_ss(int i, int mar) {
    m3[mar].ctl.cmd_ss = i;
    return;
}

void MAROC3::set_cmd_fsu(int i, int mar) {
    m3[mar].ctl.cmd_fsu = i;
    return;
}

void MAROC3::set_cmd_sum(int ch, int i, int mar) {
    int idx = 189 + 9*(63 - ch);
    int ibit8 = idx%8;
    int ibyte = idx/8;
    int bit = i == 0 ? 0:1;
    m3[mar].uc[ibyte] = (m3[mar].uc[ibyte] & (~(1 << ibit8))) | (bit << ibit8);
    return;
}

void MAROC3::set_gain(int ch, int gval, int mar) {
    // The Gain values are stored with the MSB first
    int ginv = 0;
    for (unsigned i = 0; i < 8; i++) {
        ginv <<= 1;
        ginv |= (gval & 0x1);
        gval >>= 1;
    }

    int idx = 190 + 9*(63 - ch);
    int ibit8 = idx%8;
    int ibyte = idx/8;

    if (ibit8 == 0) {  // The easy case - already byte-aligned
        m3[mar].uc[ibyte] = ginv & 0xff;
    } else {  // The hard case - value is split across byte boundary
        m3[mar].uc[ibyte+1] = (m3[mar].uc[ibyte+1] & (0xff << ibit8)) | ((ginv >> (8 - ibit8)) & 0xff);
        m3[mar].uc[ibyte] = (m3[mar].uc[ibyte] & (0xff >> (8 - ibit8))) | ((ginv << ibit8) & 0xff);
    }
    return;
}

void MAROC3::set_dac(int dac, int dval, int mar) {
// The DAC values are stored with the MSB first

    int dinv = 0;
    for (unsigned i = 0; i < 10; i++) {
        dinv <<= 1;
        dinv |= (dval & 0x1);
        dval >>= 1;
    }

// A bit fiddly - the values need to be spliced in accross byte boundary

    if ( dac == 0 ) {
        m3[mar].uc[2] = (m3[mar].uc[2] & 0x80) | ((dinv >> 3) & 0xff);
        m3[mar].uc[1] = (m3[mar].uc[1] & 0x1f) | ((dinv << 5) & 0xff);
    } else if ( dac == 1 ) {
        m3[mar].uc[1] = (m3[mar].uc[1] & 0xe0) | ((dinv >> 5) & 0xff);
        m3[mar].uc[0] = (m3[mar].uc[0] & 0x7) | ((dinv << 3) & 0xff);
    }

    return;
}

void MAROC3::set_ctest(int ch, int i, int mar) {
    int idx = 765 + 63 - ch;
    int ibit8 = idx%8;
    int ibyte = idx/8;
    int bit = i == 0 ? 0:1;
    m3[mar].uc[ibyte] = (m3[mar].uc[ibyte] & (~(1 << ibit8))) | (bit << ibit8);
    return;
}

void MAROC3::set_mask_or1(int ch, int i, int mar) {
    int ibit = 1 << ((2*(63-ch)+1)%32);
    int iu32 = (2*(63-ch)+1)/32;

    maroc3MaskOr[iu32] = i == 0 ? maroc3MaskOr[iu32] & ~ibit : maroc3MaskOr[iu32] | ibit;

    m3[mar].ctl.mask_or_e = maroc3MaskOr[0] & 0x1f;
    m3[mar].ctl.mask_or_d = ((maroc3MaskOr[0] >> 5) & 0x7ffffff) | ((maroc3MaskOr[1] & 0x1f) << 27);
    m3[mar].ctl.mask_or_c = ((maroc3MaskOr[1] >> 5) & 0x7ffffff) | ((maroc3MaskOr[2] & 0x1f) << 27);
    m3[mar].ctl.mask_or_b = ((maroc3MaskOr[2] >> 5) & 0x7ffffff) | ((maroc3MaskOr[3] & 0x1f) << 27);
    m3[mar].ctl.mask_or_a = ((maroc3MaskOr[3] >> 5) & 0x7ffffff);

    return;
}

void MAROC3::set_mask_or2(int ch, int i, int mar) {
    int ibit = 1 << ((2*(63-ch))%32);
    int iu32 = (2*(63-ch))/32;

    maroc3MaskOr[iu32] = i == 0 ? maroc3MaskOr[iu32] & ~ibit : maroc3MaskOr[iu32] | ibit;

    m3[mar].ctl.mask_or_e = maroc3MaskOr[0] & 0x1f;
    m3[mar].ctl.mask_or_d = ((maroc3MaskOr[0] >> 5) & 0x7ffffff) | ((maroc3MaskOr[1] & 0x1f) << 27);
    m3[mar].ctl.mask_or_c = ((maroc3MaskOr[1] >> 5) & 0x7ffffff) | ((maroc3MaskOr[2] & 0x1f) << 27);
    m3[mar].ctl.mask_or_b = ((maroc3MaskOr[2] >> 5) & 0x7ffffff) | ((maroc3MaskOr[3] & 0x1f) << 27);
    m3[mar].ctl.mask_or_a = ((maroc3MaskOr[3] >> 5) & 0x7ffffff);

    return;
}
