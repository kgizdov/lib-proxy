// Copyright 2016 Konstantin Gizdov University of Edinburgh

#include <stdint.h>
#include <ctype.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>
#include <vector>
#include <iostream>  // errors
#include <sstream>

#include "../include/custom.hpp"

bool checkIPAddress(const std::string &ip) {
    // check for valid IPv4
    struct sockaddr_in sa;

    if (!inet_pton(AF_INET, ip.c_str(), &(sa.sin_addr))) {
        std::cerr << "ERROR: Not a valid IP address... Aborting" << std::endl;
        return false;
    }
    return true;
}

bool checkMACAddress(const char *mac) {
    int i = 0;
    int s = 0;

    while (*mac) {
        if (isxdigit(*mac)) {
            i++;
        // } else if (*mac == ':' || *mac == '-') {  // modify to either accept dashes or not
        } else if (*mac == ':' || *mac == '-') {
            if (i == 0 || i / 2 - 1 != s)
                break;
            ++s;
        } else {
            s = -1;
        }
        ++mac;
    }

    // if (i == 12 && (s == 5 || s == 0)) {  // modify to accept/demand separators or not
    if (i == 12 && s == 5) {
        return true;  // return 1 if valid MAC
    }
    return false;
}

std::string operator+(const std::string &lstring, int int_val) {
    std::string result;
    std::stringstream temp;
    temp << int_val;
    result = lstring;
    result += temp.str();
    return result;
}

std::string operator+(const std::string &lstring, unsigned uint_val) {
    std::string result;
    std::stringstream temp;
    temp << uint_val;
    result = lstring;
    result += temp.str();
    return result;
}

int handle_join(const std::string &thread, int rc) {
    switch (rc) {
        case EDEADLK: {
            std::cerr << thread << " -> " << "ERROR: Thread was in deadlock" << std::endl;
            break;
        }
        case EINVAL: {
            std::cerr << thread << " -> " << "ERROR: Thread is not joinable" << std::endl;
            break;
        }
        case ESRCH: {
            std::cerr << thread << " -> " << "ERROR: No Thread with such ID found" << std::endl;
            break;
        }
        default: {
            std::cerr << thread << " -> " << "INFO: Thread join returned " << rc << std::endl;
            break;
        }
    }
    return rc;
}
