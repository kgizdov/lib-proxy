/* Copyright
RICH EC Control Panel
2016 Konstantin Gizdov University of Edinburgh
Steve Wotton, 2000-2015
Jackson Smith
Cavendish Lab (HEP)
*/

#include <stdint.h>
#include <iostream>

#include "../include/custom.hpp"
#include "../include/claro.hpp"

#include "../include/clarocsr.h"
#include "../include/tinyxml2.h"
#include "../include/xml_helpers.hpp"

// using byte = uint8_t;  // define byte type

const int Nregisters = 16;  // Number of CLARO per DB

byte byteArr[5];

claroControl_u_t c8[Nregisters];

void CLARO::setStateFromPreferences(Preferences topPrefs, int db ) {  // reimplement in C++
    try {
        topPrefs.sync();
    } catch (BackingStoreException &ex) { ex.printStackTrace(); }

    try {
        for ( String pd : topPrefs.childrenNames() ) {
            Preferences pdPrefs = topPrefs.node(pd);
            for ( String anode : topPrefs.node(pd).childrenNames() ) {
               Preferences cfgPrefs = Preferences.userRoot().node("RICH/CLARO/PD").node(pd).node(anode);
                if (db != cfgPrefs.getInt("db", 0)) continue;
                int feb = cfgPrefs.getInt("feb", 0);
                int asic = cfgPrefs.getInt("asic", 0);
                int ch = cfgPrefs.getInt("channel", 0);
                int claroNumber = 8*feb + asic;
                //  System.out.println(pd+" "+anode+" "+claroNumber+" "+ch);

                Preferences anodePrefs = topPrefs.node(pd).node(anode);
                boolean b = anodePrefs.getBoolean("claro.testpulse-disable", true);
                set_TPDisabled(ch, b?1:0, claroNumber);
                b = anodePrefs.getBoolean("claro.hysteresis-disable", true);
                set_hysteresisDisabled(ch, b?1:0, claroNumber);
                b = anodePrefs.getBoolean("claro.input-enable", true);
                set_inputEnabled(ch, b?1:0, claroNumber);
                int atten = anodePrefs.getInt("claro.attenuation", 0);
                set_attenuation(ch, atten, claroNumber);
                int thresh = anodePrefs.getInt("claro.threshold", 63);
                set_threshold(ch, thresh, claroNumber);
                b = anodePrefs.getBoolean("claro.threshold-offset", false);
                set_thresholdOffset(ch, b?1:0, claroNumber);

                // 'global' settings
                set_mux(pdPrefs.getInt("claro.mux", 0), claroNumber);
                b = pdPrefs.getBoolean("claro.SEU-enb", false);
                set_SEUEn(b?1:0, claroNumber);
                b = pdPrefs.getBoolean("claro.intern-corr", false);
                set_internCorr(b?1:0, claroNumber);
                b = pdPrefs.getBoolean("claro.aux-TP", false);
                set_auxTP(b?1:0, claroNumber);
                b = pdPrefs.getBoolean("claro.TP-enb", false);
                set_TPEn(b?1:0, claroNumber);
                b = pdPrefs.getBoolean("claro.SEU-reset", false);
                set_SEUreset(b?1:0, claroNumber);
            }
        }
    } catch (Exception &e) { e.printStackTrace(); }

    return;
}

void setStateFromPreferences(tinyxml2::XMLDocument *doc, tinyxml2::XMLDocument *docName, const claroPrefsStruct &commonPrefs);

int CLARO::getNregisters() {
    return Nregisters;
}

void CLARO::getRegisterArray(int regs[], int cla) {  // might not need that function
    size_t len;  // probably 16 - see note about c8[].uc[] size
    // but why isn'y the size 128 per board
    // int *array;

// Copy entire register map to the Java array

    // len = env->GetArrayLength( regs );
    len = 16;  // probably 16 as per note ^
    // array = env->GetIntArrayElements( regs, 0 );

    for (int i = 0; i < len; i++) {
        // array[i] = c8[cla].uc[i];
        regs[i] = c8[cla].uc[i];  // i needs to match to uc size - 16
    }

    // env->ReleaseIntArrayElements( regs, array, 0 );

    return;
}

void CLARO::getRegisterArray(int cla, byte regs[], int nreg, int off) {  // len -> LENGTH IS SAFE HERE
    size_t len;
    // byte *array;

// Copy to the Java array

    // len = env->GetArrayLength( regs );
    len = bufSendSize;
    // array = env->GetByteArrayElements( regs, 0 );

    for (int i = 0; (i + off < len) && (i < sizeof(claroControl_t)) && (i < nreg); i++) {
        // array[i+off] = c8[cla].uc[i];
        regs[i+off] = c8[cla].uc[i];
    }

    // env->ReleaseByteArrayElements( regs, array, 0 );

    return;
}

//=========================================================
//  'GLOBAL' VARS
//=========================================================

void CLARO::set_mux(int i, int cla) {
    c8[cla].ctl.cfg.mux_address = i & 0x7;
    return;
}

void CLARO::set_SEUEn(int i, int cla) {
    c8[cla].ctl.cfg.seu_gen_enable = i;
    return;
}

void CLARO::set_internCorr(int i, int cla) {
    c8[cla].ctl.cfg.internal_correct = i;
    return;
}

void CLARO::set_auxTP(int i, int cla) {
    c8[cla].ctl.cfg.aux_testpulse = i;
    return;
}

void CLARO::set_TPEn(int i, int cla) {
    c8[cla].ctl.cfg.testpulse_enable = i;
    return;
}

void CLARO::set_SEUreset(int i, int cla) {
    c8[cla].ctl.cfg.seu_count_reset = i;
    return;
}

//=========================================================
//  CHANNEL - SPECIFIC VARS
//=========================================================

// JNIEXPORT void CLARO::JNICALL Java_cbsw_lhcb_JniCLARO_set_1twochannels (JNIEnv *env, jobject obj, jint j, jint thresh1, jint thresh2, jint atten1, jint atten2, jint tpdis1, jint tpdis2, jint hysdis1, jint hysdis2, jint inenb1, jint inenb2, jint cla)
// {

//   // check values are inside allowed ranges
//   int atten1_corr = 0;
//   int atten2_corr = 0;
//   int thresh1_corr = 0;
//   int thresh2_corr = 0;

//   if(thresh1 < 0){thresh1_corr = 0;}
//   else if(thresh1 > 63){thresh1_corr = 63;}
//   else{thresh1_corr = thresh1;}

//   if(thresh2 < 0){thresh2_corr = 0;}
//   else if(thresh2 > 63){thresh2_corr = 63;}
//   else{thresh2_corr = thresh2;}

//   if(atten1 < 0){atten1_corr = 0;}
//   else if(atten1 > 3){atten1_corr = 3;}
//   else{atten1_corr = atten1;}

//   if(atten2 < 0){atten2_corr = 0;}
//   else if(atten2 > 3){atten2_corr = 3;}
//   else{atten2_corr = atten2;}

//   // union to hold settings for a pair of boards
//   // - gives size 3 bytes - easy to handle
//   channelpair_u_t pair;

//   // set boolean values - straightforward
//   pair.ctl.TPDis1 = tpdis1;
//   pair.ctl.TPDis2 = tpdis2;
//   pair.ctl.HysDis1 = hysdis1;
//   pair.ctl.HysDis2 = hysdis2;
//   pair.ctl.inEnb1 = inenb1;
//   pair.ctl.inEnb2 = inenb2;
//   pair.ctl.empty1 = 0;
//   pair.ctl.empty2 = 0;

//   // set atten and thresh - need to mask bits when writing, to make sure only the correct
//   // number of bits is non-zero

//   pair.ctl.atten1 = atten1_corr & 3;
//   pair.ctl.atten2 = atten2_corr & 3;
//   pair.ctl.thresh1 = thresh1_corr & 63;
//   pair.ctl.thresh2 = thresh2_corr & 63;

//   c8[cla].uc[(3*j)]   = pair.uc[0];
//   c8[cla].uc[(3*j)+1] = pair.uc[1];
//   c8[cla].uc[(3*j)+2] = pair.uc[2];

//   return;
// }

void CLARO::set_attenuation(int ch, int atten, int cla) {
    atten = (atten < 0)?0:atten;
    atten = (atten > 3)?3:atten;

    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->atten1 = atten & 3;
    if (ch%2 == 1) pair->atten2 = atten & 3;

    return;
}


void CLARO::set_threshold(int ch, int thr, int cla) {
    thr = (thr < 0)?0:thr;
    thr = (thr > 63)?63:thr;

    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->thresh1 = thr & 0x3f;
    if (ch%2 == 1) pair->thresh2 = thr & 0x3f;

    return;
}

void CLARO::set_inputEnabled(int ch, int flag, int cla) {
    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->inEnb1 = flag & 1;
    if (ch%2 == 1) pair->inEnb2 = flag & 1;

    return;
}

void CLARO::set_hysteresisDisabled(int ch, int flag, int cla) {
    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->HysDis1 = flag & 1;
    if (ch%2 == 1) pair->HysDis2 = flag & 1;

    return;
}

void CLARO::set_thresholdOffset(int ch, int flag, int cla) {
    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->thresholdOffset1 = flag & 1;
    if (ch%2 == 1) pair->thresholdOffset2 = flag & 1;

    return;
}

void CLARO::set_TPDisabled(int ch, int flag, int cla) {
    channelpair_t *pair = reinterpret_cast<channelpair_t *>(&c8[cla].ctl.chanconfig[3*(ch/2)]);

    if (ch%2 == 0) pair->TPDis1 = flag & 1;
    if (ch%2 == 1) pair->TPDis2 = flag & 1;

    return;
}
