# Proxy Library for Photon Detector Quality Assurance

Copyright &copy; 2016 Konstantin Gizdov University of Edinburgh

### Build Requirements

- TinyXML 2 - for accessing XML settings

- Digilent Adept SDK & libraries - for trigger board communication

#### Tested on Scientific Linux 6 (and up), C++03 (and up) compliant

=======

This is a low-level interface to the MAROC and CLARO boards
including the Chimaera2 trigger and backend boards.

It is normally operated by a LabView high-level UI,
but it can also communicate with other processes over STDIO (Unix pipes)
as long as the command protocol is followed. Therefore, it is a general
library for this purpose.

