#!/bin/bash

# The following line redirects error messages. Can be changed.
# Do not redirect stdin or stdout. They are used for communication with the GUI
exec 2>feb-proxy.log

FEB_PROXY_KEYFILE=/tmp/feb-proxy-keyfile
touch $FEB_PROXY_KEYFILE
sudo ./feb-proxy $@ --keyfile $FEB_PROXY_KEYFILE

echo "feb-proxy done." >&2
